/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: import/chips/p9/engd/p9n/22/p9n_ringId2Name.c $               */
/*                                                                        */
/* OpenPOWER HCODE Project                                                */
/*                                                                        */
/* COPYRIGHT 2018                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* Licensed under the Apache License, Version 2.0 (the "License");        */
/* you may not use this file except in compliance with the License.       */
/* You may obtain a copy of the License at                                */
/*                                                                        */
/*     http://www.apache.org/licenses/LICENSE-2.0                         */
/*                                                                        */
/* Unless required by applicable law or agreed to in writing, software    */
/* distributed under the License is distributed on an "AS IS" BASIS,      */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or        */
/* implied. See the License for the specific language governing           */
/* permissions and limitations under the License.                         */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
  { 0x0095BC8EAD8CCD54uLL, "perv_ana_lbst"},
  { 0x009784AD441C7051uLL, "ec_time"},
  { 0x00F06D66A34EDAE9uLL, "n1_ioo0_gptr"},
  { 0x010E312754F6C501uLL, "n0_vitl_ccvf"},
  { 0x018F8FB99578E8B4uLL, "pci1_bndy"},
  { 0x03131E514A07A612uLL, "n3_gptr"},
  { 0x034A4DE2A0996E8AuLL, "ob0_far"},
  { 0x03BDDB916067DB29uLL, "mc_pll_cmsk"},
  { 0x040CD14ACB70D735uLL, "n0_cxa0_repr"},
  { 0x04C1054DCF3B4F37uLL, "xb_io1_cmsk"},
  { 0x062E1B92BBD9D5B5uLL, "pci1_pll_bndy"},
  { 0x06A38B6AB6B261DBuLL, "ex_l3_refr_gptr"},
  { 0x06A8AE202FC4DA74uLL, "ob0_lbst"},
  { 0x06F00B3930EF621EuLL, "n0_lbst"},
  { 0x07D40E5DA53CA3B1uLL, "eq_ana_bndy"},
  { 0x081DD335682CDD65uLL, "pci0_pll_bndy"},
  { 0x082E1B1E8C213180uLL, "n3_np_time"},
  { 0x08DF796312D2C22BuLL, "eq_repr"},
  { 0x09C43002C166E7FFuLL, "xb_io2_far"},
  { 0x09C8FAEE2105E9CEuLL, "ob3_bndy"},
  { 0x0AB84BEBA5059F08uLL, "pci0_repr"},
  { 0x0B31C46E07DFE98EuLL, "xb_io2_bndy"},
  { 0x0B4F294546276AA7uLL, "xb_io2_cmsk"},
  { 0x0C98851BB3D5EE9EuLL, "ob0_repr"},
  { 0x0DC35D450AB8B8DBuLL, "xb_io2_lbst"},
  { 0x0E04BA2FB5BCA6FBuLL, "ec_bndy"},
  { 0x0E4459F7126CAA7BuLL, "xb_io0_lbst"},
  { 0x0FC27D26892EBD20uLL, "n0_time"},
  { 0x10BC2A663F1B432AuLL, "pci0_fure"},
  { 0x10DAB71C44C87E08uLL, "n3_vitl_ccvf"},
  { 0x10DF6A4CE76614E5uLL, "eq_dpll_func"},
  { 0x118189BCFEAC858EuLL, "n0_gptr"},
  { 0x11E91BAB5ADD6FB3uLL, "n3_mcs01_abst"},
  { 0x12E7E25A981D34BDuLL, "n2_lbst"},
  { 0x1312A0922DC7F8DDuLL, "mc_iom01_lbst"},
  { 0x1331936C2B94DAFEuLL, "n1_ioo0_fure"},
  { 0x137ECF1EDBAD891DuLL, "xb_io2_gptr"},
  { 0x13CFC4783F9439CEuLL, "ex_l2_gptr"},
  { 0x14FEF3913435629CuLL, "perv_ana_gptr"},
  { 0x15F57586F263422BuLL, "eq_dpll_gptr"},
  { 0x170CC3FD2D285BAFuLL, "fastarray_core"},
  { 0x170E2FDE9EF6C53DuLL, "ex_l2_mode"},
  { 0x179A4719AAD9729EuLL, "xb_io1_bndy"},
  { 0x187088A1B85AE589uLL, "xb_cmsk"},
  { 0x1A555B4322B2D411uLL, "n3_mcs01_time"},
  { 0x1BA9ED22315F58C3uLL, "ob0_cmsk"},
  { 0x1C2D7F1AF8440448uLL, "ex_l2_lbst"},
  { 0x1C87270B5E5C477DuLL, "n1_vitl_ccvf"},
  { 0x1D224AC42D868232uLL, "n0_nx_abst"},
  { 0x1D7B7EF0F40003A7uLL, "pci2_pll_gptr"},
  { 0x1DFE51C511D69AA3uLL, "n3_np_abst"},
  { 0x1E62421472AAB876uLL, "pci2_time"},
  { 0x1E8E7A1F41B9E07CuLL, "n3_np_gptr"},
  { 0x1F90C7E9AB288916uLL, "pci1_pll_lbst"},
  { 0x2048DDEFA4C19964uLL, "pci1_time"},
  { 0x231ED9766D8FE7F2uLL, "xb_gptr"},
  { 0x23B9DE24184FA775uLL, "n0_cmsk"},
  { 0x280149D334A380D9uLL, "mc_far"},
  { 0x281D02BB15BD3D7BuLL, "n3_mcs01_lbst"},
  { 0x296BE1C90476A783uLL, "n1_abst"},
  { 0x298702F8D4AAB46EuLL, "n0_far"},
  { 0x2A3BF249FD5CDBA7uLL, "eq_ana_gptr"},
  { 0x2B3DEAC6C78F7A74uLL, "pci0_time"},
  { 0x2B8B93D6F5A58497uLL, "n0_repr"},
  { 0x2C1F840B3DF01B66uLL, "n3_np_cmsk"},
  { 0x2CBFC9B8597BD017uLL, "pci2_vitl_ccvf"},
  { 0x2F6E839D64FD0280uLL, "n0_nx_repr"},
  { 0x2FC4C63A8C4E7A34uLL, "mc_gptr"},
  { 0x311B42050ED7D0C1uLL, "perv_pib_gptr"},
  { 0x3369230EC457A74CuLL, "n2_psi_bndy"},
  { 0x3398F7E10FAFC98BuLL, "mc_lbst"},
  { 0x33F3C37CA506B9CBuLL, "xb_far"},
  { 0x33F6042CCCFD7C6DuLL, "mc_iom01_cmsk"},
  { 0x34344C2F091E480AuLL, "perv_time"},
  { 0x359DF58B6722CB9AuLL, "mc_iom23_cmsk"},
  { 0x3672B33D67CD8D9AuLL, "eq_abst"},
  { 0x3777CE65D6A7E14CuLL, "n0_nx_cmsk"},
  { 0x37A80D729D184049uLL, "pci0_bndy"},
  { 0x381E12CA6D5F9E23uLL, "mc_iom23_gptr"},
  { 0x3849A458E49F3B74uLL, "xb_io0_abst"},
  { 0x3A38B4A1707711DAuLL, "pci0_inex"},
  { 0x3BEAE467803831BBuLL, "ec_inex"},
  { 0x3CACFF6D60E92138uLL, "n0_nx_lbst"},
  { 0x3D480A34CD7EE21AuLL, "n3_br_fure"},
  { 0x3D60EBD69331D34CuLL, "ob3_abst"},
  { 0x3DF2EADFD3F67E42uLL, "pci2_inex"},
  { 0x3E0CA78871E91A73uLL, "n3_np_lbst"},
  { 0x3E33E36D395FD13EuLL, "pci1_fure"},
  { 0x3FA2ACAFF5229172uLL, "n2_cxa1_lbst"},
  { 0x402A61AB4C821A40uLL, "n3_np_fure"},
  { 0x4091D7E791A93EA3uLL, "pci2_gptr"},
  { 0x4183DCDAADAFD000uLL, "xb_pll_func"},
  { 0x42DB159800B350B1uLL, "n3_far"},
  { 0x4656BD75D65B6FD6uLL, "ob3_pll_bndy"},
  { 0x4658D088E23C4B1DuLL, "perv_pib_cmsk"},
  { 0x465B322B8873528CuLL, "n3_time"},
  { 0x4667B723CC2936DDuLL, "ex_l3_time"},
  { 0x47420739DAAEF463uLL, "mc_pll_bndy"},
  { 0x478C1C9650D515F4uLL, "eq_lbst"},
  { 0x478EB7E7EF8AD758uLL, "ex_l2_time"},
  { 0x47A42ACCE74061A8uLL, "n1_mcs23_lbst"},
  { 0x483DE31DF487730CuLL, "perv_pib_time"},
  { 0x49232673FDC4F294uLL, "perv_ana_func"},
  { 0x4A3F0893B040F148uLL, "perv_far"},
  { 0x4B0A5BB0BFDDB868uLL, "n1_ioo0_repr"},
  { 0x4B615524FCC4832FuLL, "ec_vitl_ccvf"},
  { 0x4B648083B2267D66uLL, "n1_gptr"},
  { 0x4D7ACA0D0C535FA6uLL, "perv_pib_fure"},
  { 0x4D9B5DF970F561FCuLL, "eq_anadpll_lbst"},
  { 0x4E7805E7AC4401A2uLL, "mc_fure"},
  { 0x4E843F8939372A28uLL, "ex_l2_fure"},
  { 0x4ED5A88E990BC3B9uLL, "xb_io0_repr"},
  { 0x4F3EF383C61F2422uLL, "ex_l3_repr"},
  { 0x4F6F91F8D1786068uLL, "n1_ioo1_repr"},
  { 0x517EE59470DC20ECuLL, "mc_pll_lbst"},
  { 0x51CCCDE54AC7434DuLL, "perv_gptr"},
  { 0x525F98B59FED8CC7uLL, "n2_fure"},
  { 0x528F31ECF242485EuLL, "pci2_pll_bndy"},
  { 0x54C34E66F5D2C7B3uLL, "n1_mcs23_repr"},
  { 0x5597AEC8F9E6B1D8uLL, "pci2_fure"},
  { 0x55B9C5BD2552DE12uLL, "n1_ioo0_abst"},
  { 0x567D08652ADD5D81uLL, "n2_cxa1_time"},
  { 0x56E87EC52C88D76BuLL, "ex_l3_fure"},
  { 0x5787362790C2261FuLL, "ob3_fure"},
  { 0x5853EC6DBA84B283uLL, "ex_l3_cmsk"},
  { 0x588D6085359F8FB7uLL, "perv_pib_abst"},
  { 0x59B000EC5FDA922AuLL, "n0_fure"},
  { 0x5CE039A7B39F6A8FuLL, "perv_pll_cmsk"},
  { 0x5D1E372DFF66058EuLL, "n1_mcs23_far"},
  { 0x5F6B72B56498799BuLL, "n3_mcs01_fure"},
  { 0x60852EE7D1538D8FuLL, "n1_far"},
  { 0x61852960ECA56B14uLL, "xb_io0_far"},
  { 0x61DCE4A8AB08031EuLL, "ec_lbst"},
  { 0x620F57A2E25E9FECuLL, "n3_abst"},
  { 0x62911E06DB8F0C95uLL, "n1_inex"},
  { 0x62CCAAFDEAA85914uLL, "eq_inex"},
  { 0x63747FB74E46E92FuLL, "n2_psi_lbst"},
  { 0x63C4C5EE80A027E9uLL, "mc_inex"},
  { 0x64E3EADA1BB2FE22uLL, "ex_l3_refr_time"},
  { 0x66625F82F45C8F30uLL, "n3_cmsk"},
  { 0x66E8F8157448201AuLL, "mc_iom01_fure"},
  { 0x6752CB4309FDD02DuLL, "ob0_inex"},
  { 0x679377085C07A2F6uLL, "n3_mcs01_repr"},
  { 0x67E538CCE786D723uLL, "ex_l3_refr_fure"},
  { 0x68EC9302EAAB8DC2uLL, "ob0_pll_gptr"},
  { 0x6C6CD12CA9BE0A8FuLL, "xb_lbst"},
  { 0x6CC39A68F69004DEuLL, "n2_repr"},
  { 0x6D2696F93B3CD1F6uLL, "mc_pll_func"},
  { 0x6F74D23038FFC219uLL, "n1_mcs23_abst"},
  { 0x7043B51C2A985045uLL, "xb_io1_far"},
  { 0x70998E67F99689BEuLL, "n2_cmsk"},
  { 0x70B176E4F0A80900uLL, "mc_iom23_lbst"},
  { 0x727EE0CC1B3A4822uLL, "pci1_repr"},
  { 0x727F2C1ED1BABB05uLL, "mc_time"},
  { 0x72AF80081F412E76uLL, "xb_pll_lbst"},
  { 0x732C888F2852D328uLL, "xb_fure"},
  { 0x73607BA5C736E7C0uLL, "occ_fure"},
  { 0x73BEB729CD7B9408uLL, "pci1_vitl_ccvf"},
  { 0x7504F2BFBE514E96uLL, "pci0_pll_gptr"},
  { 0x75E78CEDBB0E0ABEuLL, "perv_vitl_ccfg"},
  { 0x7662B9FE5581DDB1uLL, "ec_abst"},
  { 0x7751DDEB642D6104uLL, "ex_l2_far"},
  { 0x787FC638139B698BuLL, "n3_mcs01_cmsk"},
  { 0x78FA5EADF9C03EF8uLL, "xb_io2_repr"},
  { 0x79D5BDC98958F540uLL, "eq_far"},
  { 0x7A77A9DD23FA8E76uLL, "ex_l2_repr"},
  { 0x7B2DAAB830C8F66BuLL, "perv_net_gptr"},
  { 0x7B5284A5219A18ABuLL, "xb_io0_gptr"},
  { 0x7B95E0AF75B42773uLL, "occ_repr"},
  { 0x7C44BF7111084FB6uLL, "n1_ioo1_abst"},
  { 0x7D5F6ED2F5740A1CuLL, "xb_io0_fure"},
  { 0x7DDE5A909862EC87uLL, "xb_vitl_ccvf"},
  { 0x7E00E9A6734EB289uLL, "ex_l3_refr_lbst"},
  { 0x801E6ACAD317D94CuLL, "pci2_far"},
  { 0x803BDCF71FE84AAFuLL, "n1_ioo1_gptr"},
  { 0x809AF279A1FC5CEEuLL, "n1_time"},
  { 0x80BE337816EBE78BuLL, "n3_np_repr"},
  { 0x80D7DC8DBE3278ABuLL, "ob3_inex"},
  { 0x81F91ADDA819721BuLL, "n1_fure"},
  { 0x82B66783217ABDF7uLL, "perv_pib_far"},
  { 0x832CCCE6529BCE37uLL, "occ_lbst"},
  { 0x839E7F94A68917E2uLL, "xb_io0_time"},
  { 0x84150C6442F3502CuLL, "pci2_cmsk"},
  { 0x850DDF30E554EC1DuLL, "pci2_repr"},
  { 0x8580E50DFE680B8BuLL, "eq_gptr"},
  { 0x85A0D834FF3BB1A1uLL, "mc_abst"},
  { 0x86554DA21ED93349uLL, "ex_l3_abst"},
  { 0x87A732D71EC3D7FBuLL, "n0_abst"},
  { 0x899769F587A81E19uLL, "xb_io1_abst"},
  { 0x8A11B0609E624719uLL, "n2_vitl_ccvf"},
  { 0x8A1B79EEB7200E8DuLL, "xb_pll_bndy"},
  { 0x8B241C10B54CC6CCuLL, "pci2_bndy"},
  { 0x8B9F1DE628F3EB86uLL, "ec_func"},
  { 0x8C88F3F148210733uLL, "n1_ioo1_cmsk"},
  { 0x8C9954D192A388F5uLL, "perv_repr"},
  { 0x8CE15780E42C7591uLL, "perv_pll_lbst"},
  { 0x8D682903D39F27D8uLL, "pci0_pll_lbst"},
  { 0x8E2FECAA824D3B4FuLL, "pci1_pll_gptr"},
  { 0x8EF23DD4B64676B5uLL, "perv_pib_repr"},
  { 0x8EFA81599BDACE98uLL, "n1_ioo1_lbst"},
  { 0x8F7326997B8673EBuLL, "mc_vitl_ccvf"},
  { 0x92C23F9CBEBB7706uLL, "n0_cxa0_lbst"},
  { 0x943E88E9DEA55DD3uLL, "n2_psi_fure"},
  { 0x96110523EFB76F95uLL, "n3_inex"},
  { 0x988377AACEBE06F2uLL, "n1_lbst"},
  { 0x9AC2BEB3196ACA5AuLL, "xb_io1_gptr"},
  { 0x9B38540E911F643CuLL, "ob0_vitl_ccvf"},
  { 0x9BBEDB1393019370uLL, "ec_far"},
  { 0x9D11C40B68C4B64FuLL, "ob3_pll_cmsk"},
  { 0x9D498B45967B3E62uLL, "n2_cxa1_fure"},
  { 0x9DFF00766868F27CuLL, "perv_pll_func"},
  { 0x9EA4932BDDD74583uLL, "eq_anadpll_cmsk"},
  { 0x9F09FBF38934EE09uLL, "n3_lbst"},
  { 0x9F1BA9CC964E1BB0uLL, "perv_vitl_ccvf"},
  { 0xA0564EAD95FB66B4uLL, "n2_abst"},
  { 0xA1062A599E670777uLL, "xb_io2_fure"},
  { 0xA16E9BC0B5A04201uLL, "n2_far"},
  { 0xA2B32A73E1F4C623uLL, "ex_l3_refr_repr"},
  { 0xA2CE815F9B031624uLL, "ex_l3_far"},
  { 0xA3FBBF53F3A44A70uLL, "n0_nx_far"},
  { 0xA466B434E51D58FFuLL, "pci2_abst"},
  { 0xA46E4C0F49D448ECuLL, "pci2_lbst"},
  { 0xA695445C96510947uLL, "n1_mcs23_gptr"},
  { 0xA6D3E94373B5FC39uLL, "ob0_fure"},
  { 0xA6E5899B60AA8E65uLL, "n0_nx_time"},
  { 0xA8EED6437AC98F94uLL, "n1_cmsk"},
  { 0xAA883D8E1E2BC5D5uLL, "n3_fure"},
  { 0xAB9A62EA9A06899FuLL, "ec_repr"},
  { 0xABAA7823D804BAB4uLL, "occ_time"},
  { 0xAC05C0C8F5B5E781uLL, "pci0_gptr"},
  { 0xAD271C6959244114uLL, "n1_ioo1_fure"},
  { 0xADE16661958DED06uLL, "ob0_gptr"},
  { 0xADFF7435E4FC1C5AuLL, "n1_ioo1_time"},
  { 0xAE983ED3B68414FDuLL, "ob3_repr"},
  { 0xAF8786DA98577A8CuLL, "n0_nx_gptr"},
  { 0xB14EF6F447D91B0FuLL, "pci1_inex"},
  { 0xB1AE648A96F3B968uLL, "n2_cxa1_cmsk"},
  { 0xB510F965122039BDuLL, "mc_iom01_bndy"},
  { 0xB5D9A9B40A0A7C30uLL, "ob3_vitl_ccvf"},
  { 0xB69114D79DE6D623uLL, "occ_cmsk"},
  { 0xB7B0CA0D8ED59044uLL, "perv_net_lbst"},
  { 0xBAFE2AB29AD012C0uLL, "ex_l3_refr_cmsk"},
  { 0xBB16C9F334967E83uLL, "perv_pll_gptr"},
  { 0xBC05E57B5CADC6C3uLL, "ob0_bndy"},
  { 0xBC0B105B655C412CuLL, "n2_gptr"},
  { 0xBC179321CBDD31A2uLL, "pci2_pll_lbst"},
  { 0xBC1B289A4CF0C629uLL, "ob0_pll_bndy"},
  { 0xBC373168DA19912AuLL, "pci0_lbst"},
  { 0xBE251F65DC33D002uLL, "xb_inex"},
  { 0xBFBBAD31157E4FEBuLL, "perv_net_fure"},
  { 0xBFDC55ADEE710A9FuLL, "eq_cmsk"},
  { 0xC139DB452FDB9DE5uLL, "pci1_abst"},
  { 0xC253906817B6882BuLL, "xb_io2_abst"},
  { 0xC2E2164EF01A29E0uLL, "ex_l2_cmsk"},
  { 0xC31858E7BF21433EuLL, "n2_psi_gptr"},
  { 0xC3AFDE8B57B816F1uLL, "perv_abst"},
  { 0xC460CA09F49E9244uLL, "ob3_time"},
  { 0xC4A4F376B561BE86uLL, "n1_ioo1_far"},
  { 0xC4B89076A2BDD8E0uLL, "mc_cmsk"},
  { 0xC51C6F6505106937uLL, "n2_psi_cmsk"},
  { 0xC53393AED3D6AFA9uLL, "mc_pll_gptr"},
  { 0xC6C2F759302572A0uLL, "perv_net_cmsk"},
  { 0xC814559661727EAAuLL, "n1_ioo0_time"},
  { 0xC81BEA36F6F2C30EuLL, "n0_cxa0_cmsk"},
  { 0xCA1B4EE04D271290uLL, "perv_pib_lbst"},
  { 0xCA8010AD36C64120uLL, "ec_cmsk"},
  { 0xCAC090B8DE728F2EuLL, "n3_mcs01_far"},
  { 0xCBAB05679555B858uLL, "xb_io0_cmsk"},
  { 0xCC40D739C18130B8uLL, "eq_ana_func"},
  { 0xCD01DE1A14DB8A5CuLL, "n0_cxa0_fure"},
  { 0xCD5CD08BBEE37799uLL, "perv_ana_cmsk"},
  { 0xCD6CEB2FC45180C9uLL, "pci1_gptr"},
  { 0xCDAB2404CF01973AuLL, "pci1_cmsk"},
  { 0xCECBC0B5BCBA3246uLL, "ex_l2_abst"},
  { 0xCEE5B276FC3C8222uLL, "mc_repr"},
  { 0xCF37E9F0C9F5F039uLL, "ob0_abst"},
  { 0xD00229B604D69E6EuLL, "perv_cmsk"},
  { 0xD020F9816A2A4CE8uLL, "xb_pll_gptr"},
  { 0xD13C4311707B33F8uLL, "perv_fure"},
  { 0xD205CEDF3CF767B7uLL, "ob0_time"},
  { 0xD2931282A5D8D7F0uLL, "n2_cxa1_abst"},
  { 0xD2A1C7D480F74234uLL, "ec_mode"},
  { 0xD2BF5F93DF331A04uLL, "n2_cxa1_gptr"},
  { 0xD2FC6D83EE6C96A9uLL, "ob3_pll_func"},
  { 0xD3FA85CA02AF1472uLL, "n1_ioo0_cmsk"},
  { 0xD60F71C5269DF419uLL, "mc_iom23_bndy"},
  { 0xD677876E931EFE96uLL, "pci0_abst"},
  { 0xD7F4C545CBAF5CCFuLL, "xb_io1_fure"},
  { 0xD830C9ED5ABDAFC5uLL, "xb_pll_cmsk"},
  { 0xD854549F8BD8E6E7uLL, "n1_ioo0_lbst"},
  { 0xD88A18662D90F1B5uLL, "n1_ioo0_far"},
  { 0xD9A6BB3CB33FCA9BuLL, "n0_cxa0_abst"},
  { 0xDB32414F7FD0C20CuLL, "n1_mcs23_cmsk"},
  { 0xDB4F5ABE800FF4C2uLL, "n0_cxa0_gptr"},
  { 0xDC368A62CFF1F9E1uLL, "xb_io1_lbst"},
  { 0xDCB54556D8A84FF1uLL, "occ_abst"},
  { 0xDD3569AA32DB3C18uLL, "eq_vitl_ccvf"},
  { 0xDFEC6DC2C57E8649uLL, "ob3_pll_lbst"},
  { 0xE0008FC60C61DC39uLL, "n1_repr"},
  { 0xE0075F6887EC78D4uLL, "ob3_lbst"},
  { 0xE10B00B6CB5A8A6EuLL, "ob0_pll_cmsk"},
  { 0xE2842B89E239BD3FuLL, "n0_cxa0_far"},
  { 0xE462D9B08FEAFD13uLL, "n2_cxa1_far"},
  { 0xE7449E4F73054A8CuLL, "ob3_cmsk"},
  { 0xE7CC33199E3E142CuLL, "ob3_far"},
  { 0xE8272BB249B94F10uLL, "perv_pll_bndy"},
  { 0xE91C9386FF8789B9uLL, "mc_iom23_fure"},
  { 0xE976EE8EFA70A7A4uLL, "n2_cxa1_repr"},
  { 0xE9998D6DCDD1827DuLL, "n1_mcs23_time"},
  { 0xEB3FCD1C010039CDuLL, "ob0_pll_func"},
  { 0xEBC44A97A33166EAuLL, "ex_l3_gptr"},
  { 0xECC6DA96F0ED0BADuLL, "pci1_pll_cmsk"},
  { 0xED920CB613E45B94uLL, "pci0_pll_cmsk"},
  { 0xEE4EED99F5265797uLL, "xb_time"},
  { 0xEEC7D3D42BAE75DAuLL, "n3_np_far"},
  { 0xEEE32A24B51F7111uLL, "ob3_gptr"},
  { 0xEF8050B05F9AA77EuLL, "occ_far"},
  { 0xF087CD05D6460E3DuLL, "ob3_pll_gptr"},
  { 0xF0F23932A788B30FuLL, "pci1_far"},
  { 0xF14C82363C3CB493uLL, "n2_inex"},
  { 0xF14DE0F81463CCE1uLL, "pci1_lbst"},
  { 0xF184179085FE52C4uLL, "eq_time"},
  { 0xF1BFBA2A1E689ED5uLL, "xb_repr"},
  { 0xF238C5AF649937CEuLL, "n0_nx_fure"},
  { 0xF2B26F5120F2338FuLL, "n0_inex"},
  { 0xF2EED6446E7A7EC1uLL, "perv_lbst"},
  { 0xF355D038742E972BuLL, "eq_fure"},
  { 0xF362734D2A3D1D7FuLL, "n1_mcs23_fure"},
  { 0xF4E05348A6F917D5uLL, "pci0_far"},
  { 0xF52E9EF937466DECuLL, "xb_io2_time"},
  { 0xF5521790CBE0C58AuLL, "perv_inex"},
  { 0xF56B5C046A8DEF37uLL, "n2_time"},
  { 0xF5FA6E0B7A4D52C9uLL, "n0_cxa0_time"},
  { 0xF720A5E63F47CD84uLL, "ex_l3_lbst"},
  { 0xF722D303A01A18F5uLL, "xb_io1_repr"},
  { 0xF7C395272C50C6FAuLL, "pci2_pll_cmsk"},
  { 0xF8906E895B81B563uLL, "xb_io1_time"},
  { 0xFC147096937ECD3EuLL, "ob0_pll_lbst"},
  { 0xFC2781351B0EF282uLL, "xb_abst"},
  { 0xFCB82951B47BB0BFuLL, "n3_mcs01_gptr"},
  { 0xFDC49335F9E8F06AuLL, "occ_gptr"},
  { 0xFE03AF3841E1F996uLL, "pci0_vitl_ccvf"},
  { 0xFE4E5624A60D28E0uLL, "n3_repr"},
  { 0xFED544864C839432uLL, "mc_iom01_gptr"},
  { 0xFF6754DE0311D442uLL, "pci0_cmsk"},
  { 0xFFC5FEDB83547B04uLL, "ec_gptr"},
