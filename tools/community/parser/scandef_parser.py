"""
This program is a parser for the .scandef files found in this repository.

The program can be run directly for testing purposes.
Running with no arguments causes an internal test Scanring block to be parsed
Running with 1 argument allows you to parse a file at a given path
In the first case, the resultant lark tree is printed.

Parsing a full 500MB+ scandef file is abhorrently slow with CPython. Use pypy3.

Copyright (C) 2018 Shawn Anastasio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from lark import Lark
import sys
import io

"""
The following is the lark-compatible LALR(1) grammar definition for the scandef
file format.
"""
SCANDEF_GRAMMAR = """
formatid: ("FormatId" "=" DECIMAL)
version: ("Version" "=" DECIMAL) 
root: formatid* version* scanring_block*
scanring_block: "BEGIN" "Scanring" statement+ "END"
IDENTIFIER: /[A-Za-z][A-Za-z0-9]*/
statement: assignment
         | latch_rule

assignment: IDENTIFIER "=" rvalue
?rvalue: DECIMAL 
      | HEXADECIMAL
      | "true" -> true
      | "false" -> false
      | num_arr
      | str_arr
      | STRING

bus_length: DECIMAL
fsi_offset: DECIMAL
jtag_offset: DECIMAL
inverted: DECIMAL
latch_name: /[^\ \t].+?[^\(]*/ 
latch_range: "(" DECIMAL [ ":" DECIMAL] ")"
latch_rule: bus_length fsi_offset jtag_offset inverted latch_name latch_range+

// array types
num_arr: "{" (HEXADECIMAL | DECIMAL) ("," (HEXADECIMAL | DECIMAL))* "}"
str_arr: "{" STRING ("," STRING)* "}"

// basic types
STRING_W_SPACES: STRING+
STRING: /[^\s,{}:]/+
DECIMAL: ("0".."9")+
HEXADECIMAL: ("0x" ("0".."9" | "A".."F" | "a".."f")+)

COMMENT: ("*" | "#") /[^\\n]/*
%import common.WS
%ignore WS
%ignore COMMENT
"""

"""
The following is a test scanring block that is parsed when no arguments are provided
"""
TEST_TEXT = io.StringIO(\
"""
FormatId = 2
Version = 1 * version
*******************************************************************
BEGIN Scanring
Name = ec_abst
Address = {0x20037005,0x21037005,0x22037005,0x23037005,0x24037005,0x25037005,0x26037005,0x27037005,0x28037005,0x29037005,0x2a037005,0x2b037005,0x2c037005,0x2d037005,0x2e037005,0x2f037005,0x30037005,0x31037005,0x32037005,0x33037005,0x34037005,0x35037005,0x36037005,0x37037005}
ClockDomain = {EC00,EC01,EC02,EC03,EC04,EC05,EC06,EC07,EC08,EC09,EC10,EC11,EC12,EC13,EC14,EC15,EC16,EC17,EC18,EC19,EC20,EC21,EC22,EC23}
State = Clock-Stop
Checkable = true
Broadside = true
ScanMode = setpulse-allowed
Length = 19584

*******************************************************************
*   Length of bus
*   |      FSI Offset of the first bit in bus
*   |      |      JTAG Offset of the first bit in bus
*   |      |      |   Inverted
*   |      |      |   | Name
*   |      |      |   | |
*   V      V      V   V V
*------------------------------------------------------------------
*
64       0   19520 0 TPECP.CORE.EPS.CC.COMP.SCANPCB.SCANDATAQ_REGSCAN.LATC.L2(0:63)
END
""")

class ParseError(Exception):
    pass

G_PARSER = None
def parse(file_handle):
    """
    Parses a scandef file and returns the lark object tree
    Will throw a ParseError Exception on failure

    Parameters:
    file_handle - An open File object (or compatible) corresponding to the scandef to parse

    Return:
    The root object in the parsed tree
    """
    global G_PARSER

    # Instantiate parser if required
    if G_PARSER is None:
        G_PARSER = Lark(SCANDEF_GRAMMAR, start="root", lexer="contextual", parser="lalr")
   
    try:
        res = G_PARSER.parse(file_handle.read())
    except Exception as e:
        raise ParseError("Failed to parse file: {}".format(e))

    return res

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("No file specified, running test case:")
        print(parse(TEST_TEXT).pretty())
    else:
        with open(sys.argv[1], "r", encoding="latin-1") as f:
            res = parse(f)

        # The IBM scandef files are far too big to format and print
        # only uncomment this for small files or if you have a LOT of RAM and time.
        #print(res.pretty())
