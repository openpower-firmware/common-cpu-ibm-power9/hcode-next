"""
This program is a parser for the .scomdef files found in this repository.
It currently skips "Description" and "Definition" blocks which seem to have
little use outside of documentation generation. Support for them would likely
require a custom parser, as their syntax is too ambiguous for a LALR(1) parser.

The program can be run directly for testing purposes.
Running with no arguments causes an internal test scom block to be parsed
Running with 1 argument allows you to parse a file at a given path
In both cases, the resultant lark tree is printed.

For a significant performance boost, use pypy3.

Copyright (C) 2018 Shawn Anastasio

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from lark import Lark
import sys
import io

"""
The following is the lark-compatible LALR(1) grammar definition for the scomdef
file format. It does not attempt to process Description/Definition blocks
"""
SCOMDEF_GRAMMAR = """
formatid: ("FormatId" "=" DECIMAL)
root: formatid* scom_block*
scom_block: "BEGIN" "Scom" statement+ "END" "Scom"
IDENTIFIER: /[A-Za-z][A-Za-z0-9]*/+
statement: assignment
         | description_block
         | latches_block
         | definition_block

assignment: IDENTIFIER "=" rvalue
?rvalue: DECIMAL
      | HEXADECIMAL
      | "YES"i -> yes
      | "NO"i -> no
      | num_arr
      | str_arr
      | STRING

latches_block: "BEGIN" "Latches" latch_rule* "END" "Latches"
bus_length: DECIMAL
fsi_offset: DECIMAL
jtag_offset: DECIMAL
inverted: DECIMAL
latch_name: /[^\ \t].+?[^\(]*/ 
latch_range: "(" DECIMAL [ ":" DECIMAL] ")"
latch_rule: bus_length fsi_offset jtag_offset inverted latch_name latch_range+

// Description/Definition parsing does not seem necessary for code generation, skip
_IGNORE: /[\S\xa0]/+
description_block: "BEGIN" "Description" _IGNORE* "END" "Description"
definition_block: "BEGIN" "Definition" _IGNORE* "END" "Definition"

// array types
num_arr: "{" (HEXADECIMAL | DECIMAL) ("," (HEXADECIMAL | DECIMAL))* "}"
str_arr: "{" STRING ("," STRING)* "}"

// basic types
STRING_W_SPACES: STRING+
STRING: /[^\s,{}:]/+
DECIMAL: ("0".."9")+
HEXADECIMAL: ("0x" ("0".."9" | "A".."F" | "a".."f")+)

COMMENT: "*" /[^\\n]/*
%import common.WS
%ignore WS
%ignore COMMENT
"""

"""
The following is a test Scom block that is parsed when no arguments are provided
"""
TEST_TEXT = io.StringIO(\
"""
BEGIN Scom
	Address = {0x0006C099}
	Mask = 0x0000000000000000
	Name = TP.TPCHIP.OCC.OCI.OCB.OCB_OCI_QSSR
	Spy = NO
	BEGIN Description
		OCB_OCI Quad Stop Status Register
	END Description
	ClockDomain = {pervvtl}
	State       = Clock-running
	BEGIN Latches
		32 0 0 0 TP.TPCHIP.OCC.OCI.OCB.ITPCTRL.QSSR_LT.LATC.L2(0:31)
	END Latches
	BEGIN Definition
		bits: 0:11	OCB_OCI_QSSR_L2_STOPPED(0:11)
				Bit per L2 cache in the Cache Chiplet indicating, when set to 1, that it is 
				stopped by the Stop GPE due to a Core Stop State or partial good condition. Note 
				that two Core Chiplets share each L2, so this bit implies that the Core Pair has 
				also been put into a Stop State.
		bits: 12:13	OCB_OCI_QSSR_RESERVED_12_13(0:1)
				Spare bits
		bits: 14:19	OCB_OCI_QSSR_QUAD_STOPPED(0:5)
				Bit per Quad indicating, when set to 1, that the entire Quad, meaning the Cache 
				Chiplet (and all four Core Chiplets) is stopped by the Stop GPE due to a Core 
				Stop State > 8 on all the Core Chiplets in that Quad. Per-Cache Chiplet Stop 
				STate can be found in the Quad PPM History Status register as updated by the Stop 
				GPE. Note: If QUAD_STOPPED(X) is set then L2_STOPPED(2*X : 2*X+1) must also be set.
		bits: 20:25	OCB_OCI_QSSR_STOP_ENTRY_ONGOING(0:5)
				Bit per Quad indicating, when set to 1, that the Stop GPE is processing and 
				entry into Stop state > 4. Note: Depending on the state requested, the 
				corresponding L2_STOPPED (for Stop >=8) or QUAD_STOPPED (for Stop>8) may be set when the bit 
				in the register is cleard. Also, by convention, the corresponding STOPPED bit 
				and bit in this register must never be set at the same time
		bits: 26:31	OCB_OCI_QSSR_STOP_EXIT_ONGOING(0:5)
				Bit per Quad indicating, when set to 1, that the Stop GPE is processing and 
				exit out of Stop state > 4. Note: Depending on the state requested, the 
				corresponding L2_STOPPED (for Stop >=8) or QUAD_STOPPED (for Stop>8) may be cleared when 
				the bit in the register is cleard. Also, by convention, the corresponding STOPPED 
				bit and bit in this register must always be set at the same time
	END Definition
END Scom

""")

class ParseError(Exception):
    pass

G_PARSER = None
def parse(file_handle):
    """
    Parses a scomdef file and returns the lark object tree
    Will throw a ParseError Exception on failure

    Parameters:
    file_handle - An open File object (or compatible) corresponding to the scomdef to parse

    Return:
    The root object in the parsed tree
    """
    global G_PARSER

    # Instantiate parser if required
    if G_PARSER is None:
        G_PARSER = Lark(SCOMDEF_GRAMMAR, start="root", lexer="contextual", parser="lalr")
   
    try:
        res = G_PARSER.parse(file_handle.read())
    except Exception as e:
        raise ParseError("Failed to parse file: {}".format(e))

    return res

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("No file specified, running test case:")
        print(parse(TEST_TEXT).pretty())
    else:
        with open(sys.argv[1], "r", encoding="latin-1") as f:
            res = parse(f)

        print(res.pretty())
