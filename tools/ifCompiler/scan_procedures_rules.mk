# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/scan_procedures_rules.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

###############################################################################
# BUILD_EC_SPECIFIC_WRAPPER
#    This macro iterates over all the initfiles in SCAN_INPUTS for each chip and
#        ec. One wrapper that calls all the initfile procedures for a given chip
#        and ec.
#    Order of Operation:
#       - Define the name of the generator gen_wrapper_$(chip)_$(ec)
#       - Define the sources/dependencies to generate the wrapper
#       - Define the targets $(chip)_$(ec).C/.mk
#       - Define output path, command path, and command
#       - Copy all the SCAN_INPUTS to SCAN_INPUTS_$(chip)_$(ec). This allows
#         user to specialize some inputs for a given chip and ec
#       - Define the command to execute the script
#    Input:
#       $1 == $chip == p9 or centaur
#       $2 == $chipid == p9n, p9c, cen
#       $3 == $ec
#       $4 == $mode
define BUILD_EC_SPECIFIC_WRAPPER
$(eval GENERATED=gen_wrapper_$2_$3_$4)
$(eval $(GENERATED)_PATH=$(GENPATH)/initfiles/wrapper)
$(eval SOURCES=$(ROOTPATH)/tools/perl.modules/TargetString.pm)
$(eval SOURCES+=$(ROOTPATH)/tools/ifCompiler/scan_procedures.mk)
$(eval TARGETS+=$(2)_$(3)_$(4)_wrapper.C)
$(eval TARGETS+=$(2)_$(3)_$(4)_wrapper.mk)
$(eval $(GENERATED)_COMMAND_PATH=$(ROOTPATH)/tools/ifCompiler/wrapperGeneration/)
$(eval COMMAND=generateWrapper.pl)
# Strip trailing space after chip (p9<trailing space> --> p9)
$(eval C := $(strip $(1)))
$(eval SCAN_INPUTS_$2_$3_$4 += $($C_$3_SCAN_INPUTS))
$(eval _SCAN_INPUTS=$(subst |,@,$(SCAN_INPUTS_$2_$3_$4)))
$(eval _SCAN_INPUTS+=$(subst |,@,$($(2)_$(3)_SCAN_INPUTS)))
$(eval $(2).$(4)_image_EXE_TARGETS+=$(EXEPATH)/$(2)_$(3)_$(4)_wrapper.exe)
$(eval $(GENERATED)_RUN=\
	$(call BUILD_EC_SPECIFIC_WRAPPER_COMMAND,$1,$2,$3,$4,$($(GENERATED)_PATH),$(_SCAN_INPUTS)))
$(call BUILD_GENERATED)
endef

define BUILD_EC_SPECIFIC_WRAPPER_COMMAND
		$(C1) $$$$< --chip $1 --chipId $2 --ec $3 --mode $4 --output-dir $5 $6
endef


###############################################################################
# BUILD_EC_SPECIFIC_RINGS
#    This macro runs the wrapper executable to generate rings for given chip and
#    ec
#    Order of Operation:
#       - Define the name of the generator gen_ring_$(chip)_$(ec)
#       - Define the sources/dependencies to generate the wrapper
#       - Define command path, and command
#       - Define the command to execute the wrapper
#    Input:
#       $1 == $chipId
#       $2 == $ec
#       $3 == $mode
define BUILD_EC_SPECIFIC_RINGS
$(eval GENERATED=gen_rings_$(1)_$(2)_$(3))
$(eval SOURCES+=$(GENPATH)/initfiles/wrapper/$(1)_$(2)_$(3)_wrapper.mk)
$(eval $(GENERATED)_COMMAND_PATH=$(EXEPATH)/)
$(eval COMMAND=$(1)_$(2)_$(3)_wrapper.exe)
$(eval $(GENERATED)_RUN=$(call BUILD_EC_SPECIFIC_RINGS_COMMAND,$1,$2,$3))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(IPL_BASE)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(IPL_RISK)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(IPL_CACHE_CONTAINED)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_BASE)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_RISK)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_RISK2)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_RISK3)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_RISK4)/*))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/$(RUNTIME_RISK5)/*))
$(call BUILD_GENERATED,EXE)
endef

define BUILD_EC_SPECIFIC_RINGS_COMMAND
		$(C1) $$$$<
endef
###############################################################################
# BUILD_EC_SPECIFIC_CMSK_WRAPPER
#        Generate one wrapper for each chip and ec level to generate the correct
#        cmsk rings
#    Input:
#       $1 == $chip (p9 or centaur)
#       $2 == $chipid (p9n, p9c, cen)
#       $3 == $ec 
define BUILD_EC_SPECIFIC_CMSK_WRAPPER
$(eval GENERATED=gen_cmsk_wrapper_$2_$3)
$(eval $(GENERATED)_PATH=$(GENPATH)/initfiles/wrapper)
$(eval TARGETS+=gen_cmsk_ring_$(2)_$(3).C)
$(eval TARGETS+=gen_cmsk_ring_$(2)_$(3).mk)
$(eval $(GENERATED)_COMMAND_PATH=$(ROOTPATH)/tools/ifCompiler/cmsk_wrapper/)
$(eval COMMAND=generateCmskWrapper.pl)
# Strip trailing space after chip (p9<trailing space> --> p9)
$(eval C := $(strip $(1)))
$(eval $(GENERATED)_RUN=\
	$(call BUILD_EC_SPECIFIC_CMSK_WRAPPER_COMMAND,$1,$2,$3,$($(GENERATED)_PATH)))
$(call BUILD_GENERATED)
endef

define BUILD_EC_SPECIFIC_CMSK_WRAPPER_COMMAND
		$(C1) $$$$< --chip=$1 --chipId=$2 --ec=$3 --output-dir=$4
endef

###############################################################################
# GEN_CMSK_PROCEDURE_COMMAND
#    This macro runs the command to generate the .C .H .mk and cmsk ring
#    for a given ring
#
#    Input:
#       $1 == full path to ring bin.srd file
#       $2 == full path to marker ring for $1
#       $3 == full path to stump ring
#       $4 == output path for procedure which generates cmsk ring
#       $5 == chipId
#       $6 == ec
#       $7 == mode
define GEN_CMSK_PROCEDURE_COMMAND
  $(C1) mkdir -p $4 && \
  	cp -f $1 $1.orig && \
  	cp -f $1.bitsModified $1.bitsModified.orig && \
  	$$$$< --full $1 --marker $2 --stumps $3 --cmsk $4 && \
  	cp -f -t $(ROOTPATH)/chips/p9/procedures/hwp/initfiles/$5/$6/ \
  	$4/p9_*cmsk* && cd $(ROOTPATH) && git diff --name-only HEAD~1 -- \
  	chips/p9/procedures/hwp/initfiles/$5/$6/ \
	| xargs tools/hooks/tools/pre-commit-actions > /dev/null && cd -
endef

###############################################################################
# ADD_CMSK_MARKER_FILES
#
# Create the marker file names from the marker initfiles
#
#    Input:
#       $1 ==chip Id
#       $2 ==ec
#       $3 ==mode
define ADD_CMSK_MARKER_FILES
$(eval MARKER_INITFILES=$(wildcard $(ROOTPATH)/chips/p9/initfiles/*marker.scan.initfile))
$(eval CMSK_MARKER_FILES=$(addprefix $(GENPATH)/rings/$3/$1/$2/marker/,$(addsuffix .bin.srd,\
	$(word 2,$(subst ., ,$(subst marker,ring, $(notdir $(MARKER_INITFILES))))))))
endef

###############################################################################
# ADD_CMSK_RING_TARGETS
#
# Create the generated C/H/mk file names from the passed in .bin.srd file
#
#    Input:
#       $1 == file name
#       $2 == chip id
#       $3 == ec
define ADD_CMSK_RING_TARGETS
$(eval file=$(dir $1)p9_$(basename $(basename $(subst ring,cmsk,$(notdir $1)\
			)))_$2_$3)
endef

###############################################################################
# GEN_CMSK_RINGS
#
# Run the cmsk wrapper to generate the stumped ring
#
define GEN_CMSK_RINGS
	$(C1) $$$$<
endef

###############################################################################
# BUILD_CMSK_RINGS
#    This macro runs the wrapper executable to generate CMSK rings for a given
#    ec and chip type currently only works on runtime_base rings
#    Order of Operation:
#       - Define the name of the generated file gen_cmsk_ring_$(chip)_$(ec)
#       - Define the sources/dependencies to generate the wrapper
#       - Define command path, and command
#       - Define the command to execute the wrapper
#    Input:
#       $1 == $chipId
#       $2 == $ec
#       $3 == $mode
define BUILD_CMSK_RINGS
$(eval GENERATED=gen_cmsk_ring_$(1)_$(2))
$(eval SOURCES=$(GENPATH)/.$(1).$(2).cmsk.procedure.built)
$(eval $(GENERATED)_COMMAND_PATH=$(EXEPATH)/)
$(eval COMMAND=$(1)_$(2)_cmsk_wrapper.exe)
$(eval $(GENERATED)_RUN=$(call GEN_CMSK_RINGS,$(1),$(2)))
$(eval $(call CLEAN_TARGET,$(GENPATH)/rings/$3/$1/$2/marker/*))
$(eval $(1).$(3)_image_PRE_IMAGE_TARGETS+=$$($(GENERATED)_PATH/.$(GENERATED).built))
$(call BUILD_GENERATED,PRE_IMAGE)
endef

###############################################################################
# GEN_CMSK_PROCEDURE
#
# This marco defines the steps required to generate the CMSK procedure from
# the rings file
#
#    Input:
#       $1 == $chipId
#       $2 == $ec
#       $3 == $mode
define GEN_CMSK_PROCEDURE
$(call ADD_CMSK_MARKER_FILES,$1,$2,$3)
$(foreach cmsk_file, $(CMSK_MARKER_FILES),
$(eval marker_file=$(cmsk_file))
$(call ADD_CMSK_RING_TARGETS,$(cmsk_file),$(1),$(2))
$(eval ring_file=$(subst marker,runtime_base, $(marker_file))))
$(eval stumps_dir=$(dir $(ring_file)))
$(eval output_directory=$(GENPATH)/initfiles/$1/$2)
$(eval GENERATED=$(1).$(2).cmsk.procedure)
$(eval SOURCES+=$(GENPATH)/.gen_rings_$(1)_$(2)_$(3).built)
$(eval $(GENERATED)_COMMAND_PATH=$(EXEPATH)/)
$(eval COMMAND=create_stumped_ring.exe)
$(eval $(GENERATED)_RUN=$(call GEN_CMSK_PROCEDURE_COMMAND,$(ring_file),$(marker_file),\
	$(stumps_dir),$(output_directory),$1,$2,$3))
$(eval $(1).$(3)_image_PRE_IMAGE_TARGETS+=$(GENPATH)/.$(GENERATED).built)
$(call BUILD_GENERATED,EXE)
endef

#Iterate over all the chip and ec pairs to generate a wrapper specific for that chip and ec
$(foreach chip_ec_pair,$(filter-out ocmb%, $(CHIP_EC_PAIRS)),\
	$(foreach mode,$(HW_IMAGE_VARIATIONS),\
	$(eval $(call BUILD_EC_SPECIFIC_WRAPPER\
	,$(word 1,$(subst :, ,$(chip_ec_pair)))\
	,$(word 2,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))),$(mode)))))


#Iterate over all the chip and ec to generate rings specific for that chip and ec
$(foreach chip_ec_pair,$(filter-out ocmb%, $(CHIP_EC_PAIRS)),\
	$(foreach mode,$(HW_IMAGE_VARIATIONS),\
	$(eval $(call BUILD_EC_SPECIFIC_RINGS\
	,$(word 2,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))),$(mode)))))

#only run for hw variation
$(foreach chip_ec_pair,$(filter p9%, $(CHIP_EC_PAIRS)),\
	$(eval $(call GEN_CMSK_PROCEDURE,$(word 2,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))),hw)))

$(foreach chip_ec_pair,$(filter p9%, $(CHIP_EC_PAIRS)),\
	$(eval $(call BUILD_EC_SPECIFIC_CMSK_WRAPPER\
	,$(word 1,$(subst :, ,$(chip_ec_pair)))\
	,$(word 2,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))),)))

$(foreach chip_ec_pair,$(filter p9%, $(CHIP_EC_PAIRS)),\
	$(eval $(call BUILD_CMSK_RINGS,$(word 2,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))),hw)))


