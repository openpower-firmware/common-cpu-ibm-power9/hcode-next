/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/initCompiler.lex $              */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015,2016                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
%{
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>

#include <initCompiler.H>
#include <initCompiler.tab.h>

extern "C" int yylex ();
extern "C" int yyline;
%}

%option nounput

    /* -- Basic type definitions -- */
NEWLINE  \n
COMMENT  #.*\n

LETTERS  [A-Aa-z]*
FILENAME [A-Za-z][A-Za-z0-9_\.]*
SPYNAME  [A-Za-z0-9\.\&<>\!\*#\/%\$_]+
ID       [A-Za-z0-9][A-Za-z0-9_]*

CHIP_EC_FEATURE_ATTR ATTR_CHIP_EC_FEATURE_{ID}
ATTR_NAME ATTR_{ID}
SYNTAX_DIRECTIVE  ","|";"|":"|"="|"("|")"|"["|"]"

DIGIT        [0-9]
BINARY_DIGIT [0-1]
HEX_DIGIT    [0-9A-Fa-f]

BINARY  0[bB][0-1]+
HEX     0[xX][A-Fa-f0-9]+
INTEGER {DIGIT}+

TARGETS TARGET_TYPE_{ID}
WHITESPACE [ \t]*
    /* -- define all the states here -- */
%x      spyname
%x      when_expr
%x      spy_body
%x      define
%x      target
%x      tgt_type

%%
    /* --- TOKEN DEFINITION ---- */
<*>{WHITESPACE}         { /*ignore whitespace*/ }
                        /* skip comments and new lines */
<*>{COMMENT}|{NEWLINE} {   ++yyline; }
<*>{INTEGER}        {
                        yylval.integer = atoi (yytext);
                        return INIT_INTEGER;
                    }
<*>{BINARY}         {
                        yylval.str_ptr = new std::string(yytext);
                        return INIT_BINARY;
                    }
<*>{HEX}            {
                        yylval.str_ptr = new std::string(yytext);
                        return INIT_HEX;
                    }
<*>{SYNTAX_DIRECTIVE} { return yytext [0]; }

SyntaxVersion       {
                        return INIT_SYNTAX_VERSION;
                    }
target_type         {   BEGIN(tgt_type); return INIT_TARGET_TYPE_KEYWORD;   }

<tgt_type>{TARGETS} {
                        yylval.str_ptr = new std::string(yytext);
                        BEGIN(INITIAL);
                        return INIT_TARGET_TYPE;
                     }

                    /*Map espy to 0 and ispy to 1 */
espy                {
                        yylval.integer =  0;
                        BEGIN(spyname);
                        return INIT_SPY_TYPE;
                    }
ispy                {
                        yylval.integer =  1;
                        BEGIN(spyname);
                        return INIT_SPY_TYPE;
                    }

                    /*  After espy/ispy, we go in the spyname state and try to
                     *  parse for spy name until we see the keyword "when", then
                     *  we go into when_expr state
                     */
<spyname>when       {   BEGIN(when_expr); return INIT_WHEN_KEYWORD; }
<spyname>{SPYNAME}  {
                        yylval.str_ptr = new std::string(yytext);
                        return INIT_SPY_ID;
                    }

                    /* Mapy "when =" clause to an enum. Leaving this as a case
                     * statement so we can add more in future as needeed
                     */
<when_expr>[A-Z]    {
                        switch ((char)(*yytext))
                        {
                         case 'L' :
                            yylval.integer = init_compiler::WHEN_EQUAL_L;
                            break;
                         case 'S' :
                            yylval.integer = init_compiler::WHEN_EQUAL_S;
                            break;
                        }
                        BEGIN (INITIAL);
                        return INIT_WHEN_ID;
                    }

                    /*  After the "when = " clause, we look for the "{" as the
                     *  start of the spy_body state and "}" ends the spy_body
                     *  state
                     */
<*>"{"              {   BEGIN(spy_body); return yytext [0];}
<spy_body>bits      {   return INIT_BITS_KEYWORD;   }
<spy_body>spyv      {   return INIT_SPYV_KEYWORD;   }
<spy_body>expr      {   return INIT_EXPR_KEYWORD;   }
<spy_body>any       {   return INIT_ANY_KEYWORD;    }
<*>"}"              {   BEGIN(INITIAL); return yytext[0];}

                    /* Looking {ID}. to parse out the target name associated
                     * with attributes. Generic ID can be a lot of things. So,
                     * need to look for ID. and remove the last "." before
                     * sending the data in the token.
                     * Leaving the scope to all states and these can be accessed
                     * within a define state or spy_body state
                     */
<*>{ID}\.           {
                        yylval.str_ptr = new std::string (yytext);
                        //remove the last "."
                        yylval.str_ptr->pop_back();
                        return INIT_TGT;
                    }
<*>{CHIP_EC_FEATURE_ATTR}   {
                                yylval.str_ptr = new std::string (yytext);
                                return INIT_CHIP_EC_FEATURE_ATTR_NAME;
                            }
<*>{ATTR_NAME}      {
                        yylval.str_ptr = new std::string (yytext);
                        return INIT_ATTR_NAME;
                    }
<*>"~"   { yylval.str_ptr = new std::string(yytext); return INIT_NOT;}
<*>"!"   { yylval.str_ptr = new std::string(yytext); return INIT_LOGIC_NOT;}
<*>"+"   { yylval.str_ptr = new std::string(yytext); return INIT_PLUS;}
<*>"-"   { yylval.str_ptr = new std::string(yytext); return INIT_MINUS;}
<*>"*"   { yylval.str_ptr = new std::string(yytext); return INIT_MUL;}
<*>"/"   { yylval.str_ptr = new std::string(yytext); return INIT_DIV;}
<*>"%"   { yylval.str_ptr = new std::string(yytext); return INIT_MOD;}
<*>"&&"  { yylval.str_ptr = new std::string(yytext); return INIT_LOGIC_AND;}
<*>"||"  { yylval.str_ptr = new std::string(yytext); return INIT_LOGIC_OR;}
<*>"&"   { yylval.str_ptr = new std::string(yytext); return INIT_AND;}
<*>"|"   { yylval.str_ptr = new std::string(yytext); return INIT_OR;}
<*>"^"   { yylval.str_ptr = new std::string(yytext); return INIT_XOR;}
<*>"=="  { yylval.str_ptr = new std::string(yytext); return INIT_EQ;}
<*>"!="  { yylval.str_ptr = new std::string(yytext); return INIT_NE;}
<*>"<"   { yylval.str_ptr = new std::string(yytext); return INIT_LT;}
<*>"<="  { yylval.str_ptr = new std::string(yytext); return INIT_LE;}
<*>">"   { yylval.str_ptr = new std::string(yytext); return INIT_GT;}
<*>">="  { yylval.str_ptr = new std::string(yytext); return INIT_GE;}
<*>">>"  { yylval.str_ptr = new std::string(yytext); return INIT_RSH;}
<*>"<<"  { yylval.str_ptr = new std::string(yytext); return INIT_LSH;}


                    /* Once we see the keyword "define", we go in the define
                     * state and look for the define name (def_{ID}). If the
                     * define name is not prefixed by def_, then it must be a
                     * define for targets, so, we store it as the
                     * INIT_TARGET_NAME. INIT_TARGET_NAME must be followed by
                     * the name of the target which is made up of LETTERS and
                     * NUMBERS. We need the LETTERS nad NUMBERS separate to map
                     * the numbers to the target_type indicies.
                     *
                     */
<*>define           {   BEGIN(define); return INIT_DEFINE_KEYWORD;  }
<*>def_{ID}         {
                        yylval.str_ptr = new std::string(yytext);
                        BEGIN(INITIAL);
                        return INIT_DEFINE_NAME;
                    }
<define>{ID}        {
                        yylval.str_ptr = new std::string(yytext);
                        BEGIN(INITIAL);
                        return INIT_TARGET_NAME;
                    }
<*>{ID}             {
                        yylval.str_ptr = new std::string (yytext);
                        return INIT_ID;
                    }
%%

int yywrap () {return 1;} //lexism to get yacc code to run
