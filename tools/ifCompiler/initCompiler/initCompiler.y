/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/initCompiler.y $                */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015,2018                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/**
 * @file initCompiler.y
 * @brief Contains the yacc/bison code for parsing an initfile.
 */
%{
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <initCompiler.H>
#include <plat_chip_ec_feature.H>

#define YYERROR_VERBOSE 1

extern "C" int yylex();
extern FILE* yyin;
int yyline = 1;
void yyerror(init_compiler::InitFile& i_initFile, const char * s);

%}

%parse-param {init_compiler::InitFile& i_initFile}
/* Union for the yylval variable in lex or $$ variables in bsion code.
 * Used to store the data associated with a parsed token.
 */
%union{
    uint64_t                            integer;
    std::string *                       str_ptr;
    init_compiler::BitRange_t*          init_component;
    init_compiler::Node*                init_node;
    init_compiler::spy*                 init_spy;
    init_compiler::Define*              init_define;
    init_compiler::SpyRowsMap_t*        init_row;
    std::vector<std::string>*           init_str_vector;
    std::vector<int>*                   init_int_vector;
    std::vector<init_compiler::Node*>*  init_Node_ptr_vector;
    init_compiler::ChipInfoMap_t*     init_chip_info;
}

/* indicates the name for the start symbol */
%start input

/* Define terminal symbols and the union type
 * associated with each. */

%token <integer>  INIT_INTEGER
%token <integer>  INIT_SPY_TYPE
%token <str_ptr>  INIT_SPY_ID
%token <integer>  INIT_WHEN_ID
%token <str_ptr>  INIT_BINARY_OP
%token <str_ptr>  INIT_UNARY_OP
%token <str_ptr>  INIT_ATTR_NAME
%token <str_ptr>  INIT_ID
%token <str_ptr>  INIT_TGT
%token <str_ptr>  INIT_BINARY
%token <str_ptr>  INIT_HEX
%token <str_ptr>  INIT_DEFINE_NAME
%token <str_ptr>  INIT_TARGET_NAME
%token <str_ptr>  INIT_TARGET_TYPE
%token <str_ptr>  INIT_SYS_TARGET
%token <str_ptr>  INIT_LETTERS
%token <str_ptr>  INIT_CHIP_EC_FEATURE_ATTR_NAME

/* Define terminal symbols that don't have any associated data */
%token INIT_ENDINITFILE
%token INIT_SYNTAX_VERSION
%token INIT_WHEN_KEYWORD
%token INIT_BITS_KEYWORD
%token INIT_SPYV_KEYWORD
%token INIT_EXPR_KEYWORD
%token INIT_ANY_KEYWORD
%token INIT_DEFINE_KEYWORD
%token INIT_TARGET_TYPE_KEYWORD

%token <str_ptr>INIT_PLUS
%token <str_ptr>INIT_MINUS
%token <str_ptr>INIT_MUL
%token <str_ptr>INIT_DIV
%token <str_ptr>INIT_MOD

/* Bitwise operations */
%token <str_ptr>INIT_AND
%token <str_ptr>INIT_OR
%token <str_ptr>INIT_XOR
%token <str_ptr>INIT_NOT

/*Logical operations */
%token <str_ptr>INIT_LOGIC_AND
%token <str_ptr>INIT_LOGIC_OR
%token <str_ptr>INIT_LOGIC_NOT

%token <str_ptr>INIT_EQ /*==*/
%token <str_ptr>INIT_NE /*!=*/
%token <str_ptr>INIT_LT /*<*/
%token <str_ptr>INIT_LE /*<=*/
%token <str_ptr>INIT_GT /*>*/
%token <str_ptr>INIT_GE /*>=*/

%token <str_ptr>INIT_RSH /*>>*/
%token <str_ptr>INIT_LSH /*<<*/

/* top is lowest precedent - done last */
%left  INIT_LOGIC_OR
%left  INIT_LOGIC_AND
%left  INIT_OR
%left  INIT_XOR
%left  INIT_AND
%left  INIT_EQ INIT_NE
%left  INIT_LE INIT_GE INIT_LT INIT_GT
%left  INIT_RSH INIT_LSH
%left  INIT_MINUS INIT_PLUS
%left  INIT_MUL INIT_DIV INIT_MOD
%right INIT_LOGIC_NOT INIT_NOT

/*return types of the yacc rules/states */
%type <str_ptr>                 bits
%type <init_Node_ptr_vector>    subscriptlist
%type <init_node>               subscript
%type <init_node>               expr
%type <init_node>               attribute
%type <str_ptr>                 attr_name
%type <init_row>                spydatarow_s
%type <init_row>                spydatarow_bs
%type <init_row>                spydatarow_se
%type <init_row>                spydatarow_bse
%type <init_row>                spybody
%type <init_str_vector>         spyid
%type <init_int_vector>         spy_list
%type <init_define>             define
%type <integer>                 syntax_version
%type <init_spy>                spy
%type <init_chip_info>          when_exp

%%
/* Grammars */
/* the 'input' is simply all the lines */
input:
        | input line
;

line:   syntax_version  { i_initFile.setSyntaxVersion($1);}
        | define        { i_initFile.insertDefine ($1);  }
        | target_types
        | spy           { i_initFile.insertSpy($1); }
;

syntax_version: INIT_SYNTAX_VERSION '=' INIT_INTEGER { $$ = $3; }
;

target_types: INIT_TARGET_TYPE_KEYWORD INIT_INTEGER INIT_TARGET_TYPE ';'
              {

                //All the target types are mapped to TGT# based on the
                //target_type #
                std::string* l_targetName = new std::string(std::string("TGT") +
                                                            std::to_string($2));
                bool l_ret = i_initFile.insertTargetType(*l_targetName,*$3);
                if (l_ret == false)
                {
                    yyerror(i_initFile, "Repeated index for target_type");
                }
              }
;

define:   INIT_DEFINE_KEYWORD INIT_DEFINE_NAME '=' expr ';'
          {
              $$ = new init_compiler::Define (*$2, $4);
          }
          | INIT_DEFINE_KEYWORD INIT_TARGET_NAME '='INIT_ID';'
          {
              //Ex: define CENTAUR = TGT1
              //We must have a corresponding target_type before the
              //define, otherwise throw an error
              auto l_it = i_initFile.getTargets().find(*$4);
              if (l_it == i_initFile.getTargets().end())
              {
                yyerror(i_initFile, "Unsupported target definition, must use"
                     "index to map to a target_type");
              }
              $$ = new init_compiler::Define (*$2, *$4);
          }
;

spy:      INIT_SPY_TYPE spyid'['INIT_WHEN_KEYWORD'='INIT_WHEN_ID when_exp']''{'spybody'}'
          {
              //create a spy object
              init_compiler::spy* l_pCurSpy = new init_compiler::spy(
                  $1, *$2, (init_compiler::WhenId)$6, $10);

              l_pCurSpy->setSupportedChipEcLevels($7);

              $$ = l_pCurSpy;

              //Debug Traces
#ifdef INITCOMPILER_DEBUG
                for (auto l_id: *$2)
                {
                    std::cout << "SpyName: " << l_id << std::endl;
                }

                for (auto l_map : *$10)
                {
                    auto l_bitRange  = l_map.first;
                    auto l_RowVector = l_map.second;
                    for (auto& l_row : l_RowVector)
                    {
                         std::cout <<"BitRange: ("<< l_bitRange  << ")"
                         << "  Spyv: " << l_row->getSpyv()->toString()
                         << "  Expr: " << l_row->getExpr()->toString()
                         << std::endl;
                    }
                }

                std::cout << "getSupportedChipEcLevels chip size: " <<
                    l_pCurSpy->getSupportedChipEcLevels()->size() << std::endl;

                for (auto l_pair : *(l_pCurSpy->getSupportedChipEcLevels()))
                {
                    std::cout << "getSupportedChipEcLevels ec size: " <<
                        l_pair.second.size() << std::endl;
                    for (auto l_ec : l_pair.second)
                    {
                        std::cout << "Chip: " << l_pair.first
                                  << " EC: " << l_ec << std::endl;
                    }
                }
                std::cout << std::endl;

                //std::map<std::string, std::vector<Row*> > 
                auto l_rm = l_pCurSpy->getRowsMap();
                std::cout << "getRowsMap size: " << l_rm->size() << std::endl;
                for (auto l_mp = l_rm->begin(); l_mp!=l_rm->end(); ++l_mp) {
                  std::cout << "  <> " << (l_mp->first) << std::endl;
                  auto l_v = l_mp->second;
                  for (auto l_rw = l_v.begin(); l_rw!=l_v.end(); ++l_rw) {
                    std::cout << "     * " << (*l_rw)->getSpyv()->toString() << ", " << (*l_rw)->getExpr()->toString() << std::endl;
                  }
                }

#endif
          }
;

spyid:      INIT_SPY_ID
            { /*create and return a pointer to a vector of string*/
                auto l_str_vector = new std::vector<std::string>();
                l_str_vector->emplace_back(*$1);
                $$ = l_str_vector;
            }
            | spyid '(' spy_list ')'
            {
                $$ = init_compiler::processSpyId ($1, $3, std::string());
            }
            | spyid '(' spy_list ')' INIT_SPY_ID
            {
                $$ = init_compiler::processSpyId ($1, $3, *$5);
            }
;

spy_list:   INIT_INTEGER
            {
                auto l_int_vector = new std::vector<int> ();
                l_int_vector->emplace_back($1);
                $$ = l_int_vector;
            }
            | spy_list ',' INIT_INTEGER
            {
                $1->emplace_back ($3);
                $$ = $1;
            }
;
            /* this is expression after "when=", it can
             * be empty or have "&& (expr with ATTR_CHIP_EC_FEATURE)"
             */
when_exp:       {
                    $$ = init_compiler::getSupportedChipEcLevels(i_initFile,std::string(),std::string());
                }
            |   INIT_LOGIC_AND INIT_CHIP_EC_FEATURE_ATTR_NAME
                {
                    $$ = init_compiler::getSupportedChipEcLevels(i_initFile, *$2,std::string("false"));
                }
            |   INIT_LOGIC_AND INIT_LOGIC_NOT INIT_CHIP_EC_FEATURE_ATTR_NAME
                {
                    $$ = init_compiler::getSupportedChipEcLevels(i_initFile, *$3, std::string("true"));
                }


spybody:    spybodyheader_s     spydatarow_s   { $$ = $2; }
            | spybodyheader_se  spydatarow_se  { $$ = $2; }
            | spybodyheader_bs  spydatarow_bs  { $$ = $2; }
            | spybodyheader_bse spydatarow_bse { $$ = $2; }
;

spybodyheader_s:   INIT_SPYV_KEYWORD ';'
;

spybodyheader_bs:  INIT_BITS_KEYWORD ',' INIT_SPYV_KEYWORD ';'
;

spybodyheader_se:  INIT_SPYV_KEYWORD ',' INIT_EXPR_KEYWORD ';'
;

spybodyheader_bse: INIT_BITS_KEYWORD','INIT_SPYV_KEYWORD','INIT_EXPR_KEYWORD ';'
;

                /*Since, spyv column can also contain an expression,
                we treat both expr and spyv column the same */
spydatarow_s:   expr ';' /*spybody with only spyv column*/
                {
                    auto l_node = new init_compiler::Node();
                    $$ = init_compiler::createMap(":", $1, l_node);
                }
;

spydatarow_bs:  bits ',' expr ';' /* spybody with bits and spyv */
                {
                    auto l_node = new init_compiler::Node();
                    $$ = init_compiler::createMap(*$1, $3, l_node);
                }
                | spydatarow_bs bits ',' expr ';'
                {
                    auto l_node = new init_compiler::Node();
                    init_compiler::insertInMap(*$1, *$2, $4, l_node);
                    $$ = $1;
                }
;

spydatarow_se:  expr ',' expr ';' /*spybody with spyv and expr*/
                {
                    $$ = init_compiler::createMap(":", $1, $3);
                }
                | spydatarow_se expr ',' expr ';'
                {
                    init_compiler::insertInMap(*$1, ":", $2, $4);
                    $$ = $1;
                }
;

spydatarow_bse: bits   ',' expr ',' expr ';' /*spybody with bits, spyv, expr*/
                {
                    $$ = init_compiler::createMap(*$1, $3, $5);
                }

                | spydatarow_bse bits ',' expr ',' expr ';'
                {
                    init_compiler::insertInMap(*$1, *$2, $4, $6);
                    $$ = $1;
                }
;

bits:           INIT_INTEGER ':' INIT_INTEGER
                {
                    $$ = new std::string
                        (std::to_string($1) + ":" + std::to_string($3));
                }
                | INIT_INTEGER
                {
                    $$ = new std::string
                        (std::to_string($1) + ":" + std::to_string($1));
                }
;

expr: attribute
      | INIT_INTEGER {
                        auto l_lit = new init_compiler::Literal (std::to_string($1));
                        $$ = i_initFile.insertLiteral(l_lit->getSymbol(),l_lit);
                     }
      | INIT_ID      {$$ = new init_compiler::Enum (*$1);}
      | INIT_HEX     {
                        auto l_lit = new init_compiler::Literal (*$1);
                        $$ = i_initFile.insertLiteral(l_lit->getSymbol(),l_lit);
                     }
      | INIT_BINARY  {
                        auto l_lit = new init_compiler::Literal (*$1);
                        $$ = i_initFile.insertLiteral(l_lit->getSymbol(),l_lit);
                     }
      | INIT_ANY_KEYWORD     {$$ = new init_compiler::Expression
                                 (new init_compiler::Node(), new init_compiler::Node(),
                                  std::string("true"));
                             }
      | INIT_DEFINE_NAME     {
                                $$ = i_initFile.getDefine(*$1);
                                if ($$ == NULL)
                                {
                                    yyerror(i_initFile, "define statement not found");
                                }
                             }
      | INIT_NOT        expr {
                                $$ = new init_compiler::Expression
                                     (new init_compiler::Node(),$2,*$1);
                             }
      | INIT_LOGIC_NOT  expr {
                                $$ = new init_compiler::Expression
                                     (new init_compiler::Node(),$2,*$1);
                             }
      | expr INIT_PLUS  expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_MINUS expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_MUL   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_DIV   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_MOD   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_AND   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_OR    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_XOR   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_LOGIC_AND expr{$$=new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_LOGIC_OR  expr{$$=new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_EQ    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_NE    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_LT    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_LE    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_GT    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_GE    expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_RSH   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | expr INIT_LSH   expr {$$ = new init_compiler::Expression($1,$3,*$2);}
      | '(' expr ')'       {$$ = $2;}
;

attribute:  INIT_TGT attr_name ':'':' INIT_ID
            {
                //This is an attribute enum. We don't care about target
                std::string l_str = "fapi2::ENUM_" + *$2 + "_" + *$5;
                $$ = new init_compiler::Enum(l_str);
            }
            | attr_name ':'':' INIT_ID
            {
                std::string l_str = "fapi2::ENUM_" + *$1 + "_" + *$4;
                $$ = new init_compiler::Enum(l_str);
            }
            | INIT_TGT attr_name subscriptlist
            {
                std::string l_tgt = *$1;
                if ($1->compare(0,3, "TGT") != 0)
                {
                    auto l_def = i_initFile.getDefine(*$1);
                    if (l_def == NULL)
                    {
                        yyerror(i_initFile, "target not found");
                    }
                    l_tgt = l_def->getTarget();
                }

                $$ = new init_compiler::Attribute (*$2, $3, l_tgt);
            }
            | INIT_ATTR_NAME subscriptlist
            {
                $$ = new init_compiler::Attribute(*$1, $2);
            }
            | INIT_CHIP_EC_FEATURE_ATTR_NAME subscriptlist
            {
                auto l_targetMap = i_initFile.getTargets();
                std::string l_chipTarget;

                for (auto i = l_targetMap.begin(); i != l_targetMap.end(); ++i)
                {
                    if( std::string(i->second) == "TARGET_TYPE_PROC_CHIP" ||
                         std::string(i->second) == "TARGET_TYPE_MEMBUF_CHIP")
                    {
                        l_chipTarget = i->first;
                    }
                }

                $$ = new init_compiler::Attribute(*$1, $2, l_chipTarget);
            }
;

attr_name:      INIT_ATTR_NAME { $$ = $1;}
                | INIT_CHIP_EC_FEATURE_ATTR_NAME { $$ = $1}

subscriptlist:  {
                    auto l_vect = new std::vector<init_compiler::Node*> ();
                    l_vect->push_back (new init_compiler::Node ());
                    $$ = l_vect;
                }
             | subscriptlist subscript
             {
                $1->push_back($2);
                $$ = $1;
             }
;
subscript:   '[' expr ']'
            {
                $$ = $2;
            }
;
%%

init_compiler::SpyRowsMap_t* init_compiler::createMap (
                                std::string i_bits,
                                init_compiler::Node* i_spyv,
                                init_compiler::Node* i_expr)
{
    auto l_rowMap = new init_compiler::SpyRowsMap_t ();
    std::vector<init_compiler::Row*> l_rowVector;

    l_rowVector.push_back(new init_compiler::Row(i_spyv, i_expr));
    l_rowMap->insert(BitRowPair_t (i_bits, l_rowVector));
    return l_rowMap;
}

void init_compiler::insertInMap (init_compiler::SpyRowsMap_t& io_map,
                                 std::string i_bits,
                                 init_compiler::Node* i_spyv,
                                 init_compiler::Node* i_expr)
{
    //try inserting a new entry in the map
    std::vector<init_compiler::Row*> l_rowVector;

    auto l_row = new init_compiler::Row(i_spyv, i_expr);
    l_rowVector.push_back(l_row);

    auto l_retVal = io_map.insert(BitRowPair_t (i_bits, l_rowVector));

    if (l_retVal.second == false)
    {
        //This bit pair already existed in the map
        //get the vector associated with this bit string
        //Add new row to the existing vector

        //l_retVal.first = map iterator pointing to the location of existing
        //entry in the map.
        l_retVal.first->second.push_back(
            new init_compiler::Row(i_spyv, i_expr));

    }
}

std::vector<std::string>* init_compiler::processSpyId
                                        (std::vector<std::string>* i_spyBase,
                                        std::vector<int>* i_spyMiddle,
                                        std::string i_spySuffix)
{
    auto l_result = new std::vector<std::string> ();
    int i = 0;
    for (auto l_spyBase : *i_spyBase)
    {
        for (auto l_spyMiddle : *i_spyMiddle)
        {
            std::stringstream l_sstm;
            l_sstm << l_spyBase << l_spyMiddle << i_spySuffix;
            l_result->emplace_back(std::string (l_sstm.str()));
            i++;
        }
    }
    return l_result;

}

// use the ec feature attribute to define the supported EC levels for
// this particular spy
init_compiler::ChipInfoMap_t* init_compiler::getSupportedChipEcLevels
             (init_compiler::InitFile& i_initFile, std::string i_attr_name, std::string invert)
{
    auto l_supportedLevel = new init_compiler::ChipInfoMap_t ();

    // grab chip/ec info passed in from commandline
    auto l_chipInfo       = i_initFile.getChipInfo();

    for (auto l_chipEcPair : l_chipInfo)
    {
        auto l_ecSet   = new std::set<std::string> ();
        auto l_chipId     = l_chipEcPair.first;
        auto l_chipIdNum  = init_compiler::chipTypeMap[l_chipId];

        for (auto l_ec : l_chipEcPair.second)
        {
            auto l_chipEc = strtol(l_ec.c_str(), NULL, 16);

            // if there is no attribue, assume its supported, otherwise
            // check the attribute and invert the answer if requested
            bool l_ecSupported = (i_attr_name.empty()) ? true :
                    ((invert =="true") ?
                     !(fapi2::hasFeature(i_attr_name,l_chipIdNum, l_chipEc)) : \
                      fapi2::hasFeature(i_attr_name,l_chipIdNum, l_chipEc));

            if (l_ecSupported)
            {
                l_ecSet->insert(l_ec);
            }
        }

        if (l_ecSet->size() > 0)
        {
            // We want to try inserting only if the ecVector's size > 0
            // because we can have a case where the spy's supported ec level
            // is not supported by initCompiler currently. In that case, we
            // want to return a zero size for the spy's supported ec level
            // map.
            auto l_retVal = l_supportedLevel->insert({l_chipId, *l_ecSet});
            if (l_retVal.second == false)
            {
                // we can safely assume that the chip won't repeat as we are
                // iterating over a map that is indexed by chip id
                yyerror (i_initFile, "Repeated chip when determining supported"
                        " chip and ec levels for a specific spy");
            }
        }
    }
#ifdef INITCOMPILER_DEBUG
    for (auto l_pair : *l_supportedLevel)
    {
        for (auto l_ec : l_pair.second)
        {
            std::cout << "getSupportedChipEcLevels> Chip: " << l_pair.first
                      << " EC: " << l_ec << std::endl;
        }
    }
#endif
    return l_supportedLevel;
}

void yyerror(init_compiler::InitFile& i_file, const char * s)
{
    std::cerr << "ERROR: " <<
        i_file.getFilename () <<
        "(Line: " << yyline << "): " << s << "\n" << std::endl;
    exit(-1);
}

std::map <uint8_t, std::string> init_compiler::chipIdMap =
{
    {0x0,""    },
    {0x3,"cen" },
    {0x5,"p9n" },
    {0x6,"p9c" },
    {0x7,"p9a" },
    {0x8,"explorer" }
};

std::map <std::string, uint8_t> init_compiler::chipTypeMap =
{
    {"",    0x0},
    {"cen", 0x3},
    {"p9n", 0x5},
    {"p9c", 0x6},
    {"p9a", 0x7},
    {"explorer", 0x8}
};
