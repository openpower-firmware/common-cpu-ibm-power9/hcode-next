# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/initCompiler/initCompiler.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

#Makefile that defines how we build the initCompiler executable.
#First, we build bison, then flex, then compile the generated files from
#bison and flex with a wrapper and header files.

#BUILD_BISON
#    This macro will generate all the recipes for compiling a bison file
#        Input: a relative path to the bison file (name.y)
#        Output: name.tab.C and name.tab.h in output/gen/initCompiler directory
#    Order of operation:
#        - Define the name of the generator: build_bison_name_of_the_file
#        - Define the command to run to compile the bison file
#        - Define the source files
#        - Define the target files
#        - Define the path to the output files
#        - call BUILD_GENERATED which runs the right recipes to compile this
#        file
define BUILD_BISON
$(eval GENERATED=build_bison_$(notdir $1))
$(eval build_bison_$(notdir $1)_RUN=$(call BUILD_BISON_RUN_COMMAND,$1,$(GENERATED)))
$(eval SOURCES+=$1)
$(eval SOURCES+=$(GENPATH)/fapi2_ifCompiler/fapi2_chip_ec_feature.H)
$(eval SOURCES+=$(GENPATH)/fapi2_ifCompiler/plat_chip_ec_feature.H)
$(eval SOURCES+=$(addprefix $(GENPATH)/fapi2_ifCompiler/,$(FILES_TO_SYMLINK)))
$(eval $(call BUILD_BISON_TARGETS,$(subst .y,,$(notdir $1))))
$(eval $(GENERATED)_PATH=$(GENPATH)/initCompiler)
$(eval $(GENERATED)_COMMAND=bison)
$(call BUILD_GENERATED)
endef

#BUILD_BISON_TARGETS
#    This macro adds target files to the TARGET variable from the
#        name of the generator
define BUILD_BISON_TARGETS
TARGETS += $(addsuffix .tab.C,$1)
TARGETS += $(addsuffix .tab.h,$1)
endef

#BUILD_BISON_RUN_COMMAND
#    This macro defines the command to use when compiling bison code
#    Order of operation
#       - cd output/gen/initCompiler directory
#       - Run the bison command on the bison file with absolute path
#       - Rename the generated to file to use .C extension. So, we can compile
#       it with C++ compiler
define BUILD_BISON_RUN_COMMAND
		$(C1) cd $$$$($2_PATH) && bison -d $(realpath $1) && rename .c .C *.tab.c
endef

#Define sources for BUILD_BISON macro
BUILD_BISON_SOURCE = $(ROOTPATH)/tools/ifCompiler/initCompiler/initCompiler.y
$(foreach src,$(BUILD_BISON_SOURCE),$(eval $(call BUILD_BISON,$(src))))

#BUILD_FLEX
#    This macro will generate all the recipes for compiling a flex file
#        Input: a relative path to the flex file (name.lex)
#        Output: lex.yy.C in output/gen/initCompiler directory
#    Order of operation:
#        - Define the name of the generator: build_flex_name_of_the_file
#        - Define the command to run to compile the flex file
#        - Define the source files
#        - Define the target files
#        - Define the path to the output files
#        - call BUILD_GENERATED which runs the right recipes to compile this
#        file
define BUILD_FLEX
$(eval GENERATED=build_flex_$(notdir $1))
$(eval build_flex_$(notdir $1)_RUN=$(call BUILD_FLEX_RUN_COMMAND,$1,$(GENERATED)))
$(eval SOURCES+=$1)
$(eval SOURCES+=$(GENPATH)/fapi2_ifCompiler/fapi2_chip_ec_feature.H)
$(eval SOURCES+=$(GENPATH)/fapi2_ifCompiler/plat_chip_ec_feature.H)
$(eval SOURCES+=$(addprefix $(GENPATH)/fapi2_ifCompiler/,$(FILES_TO_SYMLINK)))
$(eval SOURCES+=$(GENPATH)/initCompiler/.build_bison_initCompiler.y.built)
$(eval TARGETS+=lex.yy.C)
$(eval $(GENERATED)_PATH=$(GENPATH)/initCompiler)
$(eval $(GENERATED)_COMMAND=flex)
$(call BUILD_GENERATED)
endef


#BUILD_FLEX_RUN_COMMAND
#    This macro defines the command to use when compiling flex code
#    Order of operation
#       - cd output/gen/initCompiler directory
#       - Run the flex command on the flex file with absolute path
#       - Rename the generated to file to use .C extension. So, we can compile
#       it with C++ compiler
define BUILD_FLEX_RUN_COMMAND
		$(C1) cd $$$$($2_PATH) && flex $(realpath $1) && rename .c .C *.yy.c
endef

#Commands to invoke BUILD_FLEX macro
BUILD_FLEX_SOURCE = $(ROOTPATH)/tools/ifCompiler/initCompiler/initCompiler.lex
$(foreach src,$(BUILD_FLEX_SOURCE),$(eval $(call BUILD_FLEX,$(BUILD_FLEX_SOURCE))))

#BUILD_INITCOMPILER
#    This macro generates all the recipes needed to generate initCompiler
#    executable
#    Order of Operation:
#        - Define the name of the executable
#        - Define all the .o files needed to generate the .exe
#        - Define the cross compiler to use
#        - Add extra libraries needed (engineering data parser libraries)
#        - Add all the include/src paths
#        - Call BUILD_EXE macro, which will run the right recipes to generate
#        the exe
define BUILD_INITCOMPILER
$(eval EXE=initCompiler)
# DO THIS TO ENABLE DEBUG (eval $(EXE)_CXXFLAGS+=-DINITCOMPILER_DEBUG)
$(eval $(EXE)_LIBPATH=$(LIBPATH)/ifCompiler)
$(eval $(EXE)_DEPLIBS+=engd_parser)
$(eval OBJS+= initCompiler.tab.o lex.yy.o)
$(eval OBJS+=$(notdir $(patsubst %.C,%.o,\
	$(wildcard $(ROOTPATH)/tools/ifCompiler/initCompiler/*.C))))
$(eval $(EXE)_TARGET=HOST)
$(eval $(EXE)_DEPS+=$(ROOTPATH)/output/gen/initCompiler/.build_flex_initCompiler.lex.built)
$(eval $(EXE)_DEPS+=$(ROOTPATH)/output/gen/initCompiler/.build_bison_initCompiler.y.built)
$(eval $(EXE)_EXTRALIBS+=$(ECMD_REQUIRED_LIBS))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/tools/ifCompiler/plat/))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/tools/ifCompiler/engd_parser/include))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/hwpf/fapi2/include))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(GENPATH)/fapi2_ifCompiler/))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ECMD_PLAT_INCLUDE)))
$(eval $(call ADD_EXE_SRCDIR,$(EXE),$(GENPATH)/initCompiler))
$(eval $(call ADD_EXE_SRCDIR,$(EXE),$(ROOTPATH)/tools/ifCompiler/initCompiler/))
$(eval $(call BUILD_EXE))
endef
