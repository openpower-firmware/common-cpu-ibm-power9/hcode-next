/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/initCompiler.H $                */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015,2017                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#ifndef _INIT_COMPILER_H
#define _INIT_COMPILER_H

#include <initFile.H>
#include <spy.H>
#include <Scan.H>
#include <Scom.H>
#include <Row.H>
#include <Node.H>
#include <Expression.H>
#include <Literal.H>
#include <Enum.H>
#include <Attribute.H>
#include <Define.H>
#include <initCompiler_common.H>

namespace init_compiler
{

extern std::map <std::string, uint8_t> chipTypeMap;
extern std::map <uint8_t, std::string> chipIdMap;
// These are implemented in initCompiler.y.
/*
 * @brief: For each spy a map of rows is created
 * @param[in]: i_bits: string of bit pair
 * @param[in]: i_spyv: reference to a Node containing the spy value expression
 * @param[in]: i_expr: reference to a Node containing the expression to evaluate
 *                     the spy value on
 * @retval: a pointer to the SpyRowsMap_t
 */
SpyRowsMap_t* createMap (std::string i_bits, Node* i_spyv,
                         Node* i_expr);

/*
 * @brief: For each new row in the spy, an entry is made in the map
 *         If the bit pair exist in the map,
 *               a new Row is added to the existing vector
 *         If bit pair doesn't exist, a new entry is added in the map
 * @param[in]: io_map: pointer to the existing map to insert the entry in
 * @param[in]: i_bits: string of bit pair
 * @param[in]: i_spyv: reference to a Node containing the spy value expression
 * @param[in]: i_expr: reference to a Node containing the expression to evaluate
 *                     the spy value on
 */
void insertInMap (SpyRowsMap_t& io_map, std::string i_bits,
                  Node* i_spyv, Node* i_expr);

/*
 * @brief: generate all enumerations of the spy name if multiplexed name
 *         syntax is used
 * @param[in]: i_base: a vector of string pointing to the base of the spy name
 * @param[in]: i_spyMiddle: a vector of ints containing the enumerations to add
 *                          to the spyname
 * @param[in]: i_suffix: string containing the suffix of the spy name
 *
 * @retval: a pointer to the vector of strings containing all enumerations of
 *          the spy name
 */
std::vector<std::string>* processSpyId (std::vector<std::string>* i_spyBase,
                                        std::vector<int>* i_spyMiddle,
                                        std::string i_spySuffix);

/*
 * @brief: returns a map containing all the supported chip and ec
 *         levels for a specific spy by querying the ATTR_CHIP_EC_FEATURE's
 *         hasFeature function
 * @param[in]: string containing the ATTR_CHIP_EC_FEATURE to querry
 * @retval:    a map containing the chip and ec values
 */
ChipInfoMap_t* getSupportedChipEcLevels (InitFile& i_initfile, std::string i_attr_name, std::string);

};
#endif
