# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/plat/fapi2CreateAttrGetSetMacros.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
GENERATED=createPlatAttrService_ifCompiler
$(GENERATED)_COMMAND_PATH=$(ROOTPATH)/tools/ifCompiler/plat/
COMMAND=fapi2CreateAttrGetSetMacros.pl

SOURCES+=$(GENPATH)/attribute_ids.H
TARGETS+=plat_attribute_service.H

$(GENERATED)_PATH=$(GENPATH)/fapi2_ifCompiler

define createPlatAttrService_ifCompiler_RUN
		$(C1) $$< -i $(GENPATH) -o $$($(GENERATED)_PATH)
endef
$(call BUILD_GENERATED)
