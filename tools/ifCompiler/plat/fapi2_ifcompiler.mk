# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/plat/fapi2_ifcompiler.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

#Makefile to generate libfapi2_ifCompiler.so

#Commands to build libfapi2_ifCompiler
MODULE = fapi2_ifcompiler
OBJS += $(FAPI2_MODULE_OBJS)
OBJS += plat_utils.o
OBJS += fapi2_utils.o
lib$(MODULE)_COMMONFLAGS += -DFAPI_SUPPORT_SPY_AS_STRING=1
lib$(MODULE)_LIBPATH=$(LIBPATH)/ifCompiler
$(call ADD_MODULE_SRCDIR,fapi2_ifcompiler,$(ROOTPATH)/tools/ifCompiler/plat)
$(call ADD_MODULE_SRCDIR,fapi2_ifcompiler,$(FAPI2_PATH)/src)
$(call ADD_MODULE_SRCDIR,fapi2_ifcompiler,$(GENPATH)/fapi2_ifCompiler)
$(call ADD_MODULE_INCDIR,fapi2_ifcompiler,$(ROOTPATH)/tools/ifCompiler/plat)
$(call ADD_MODULE_INCDIR,fapi2_ifcompiler,$(FAPI2_PATH)/include)
$(call BUILD_MODULE)
