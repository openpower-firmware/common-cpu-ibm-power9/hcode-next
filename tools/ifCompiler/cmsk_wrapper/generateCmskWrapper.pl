#!/usr/bin/perl
# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/cmsk_wrapper/generateCmskWrapper.pl $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2017,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

use strict;
use File::Basename qw(fileparse);
use File::Path qw(make_path);
use Getopt::Long;
use Data::Dumper qw(Dumper);

use lib "$ENV{'PERLMODULES'}";
use TargetString;

my $chip      = undef;
my $chipId    = undef;
my $ec        = undef;
my $outputDir = undef;
my $debug     = undef;
my $rootpath  = "../..";

my %RING_ID_FILE = ( "p9" => "p9_ringId.H", );

my %CHIP_TYPE = (
    "cen" => "fapi2::ENUM_ATTR_NAME_CENTAUR",
    "p9n" => "fapi2::ENUM_ATTR_NAME_NIMBUS",
    "p9c" => "fapi2::ENUM_ATTR_NAME_CUMULUS",
    "p9a" => "fapi2::ENUM_ATTR_NAME_AXONE",
);

GetOptions(
    "chip=s"       => \$chip,
    "chipId=s"     => \$chipId,
    "ec=s"         => \$ec,
    "debug=s"      => \$debug,
    "output-dir=s" => \$outputDir
);

my $debug = 1;

#print out all the input arguments for debug purposes

if ($debug)
{
    print "chipId       = $chipId\n";
    print "ec           = $ec\n";
    print "debug        = $debug\n";
    print "output-dir   = $outputDir\n";

    foreach my $argNum ( 0 .. $#ARGV )
    {
        my $temp = $ARGV[$argNum];
        $temp =~ s/\@/ /g;
        print "$temp\n";
    }
}

my $numArgs = @ARGV + 1;

#Error checking on command-line argument
if (   ( !defined $chipId )
    || ( !defined $ec )
    || ( !defined $chip )
    || ( !defined $outputDir )
    || ( $numArgs < 1 ) )
{
    print "Usage: generateWrapper.pl --chip=<chipid> --ec=<chipec>
        --output-dir=<output dir>\n";
    exit(1);
}

#globals
my $attrPath = $rootpath . "/chips/$chip/initfiles/attribute_ovd/";

#call the main function
main();

###############################################################################
# Subroutines Begin
###############################################################################
sub main
{
    #open the *wrapper.C file for writing
    my $outFile = $chipId . "_" . $ec . "_cmsk_wrapper";
    open( OUTFILE, ">", $outputDir . "/" . $outFile . ".C" );
    displayWrapperBeginning();
    my $procedureName = "p9_ec_func_cmsk_" . $chipId . "_" . $ec;
    displayWrapperBody( $chipId, $ec, $procedureName );
    print OUTFILE "}\n";
    close(OUTFILE);

    #open *wrapper.mk for writing
    open( OUT_MK_FILE, ">", $outputDir . "/" . $outFile . ".mk" );
    writeMkFile( $chipId, $ec, $outFile );

}

#sort a name by numbers
sub sort_by_number
{
    $a =~ /(\d+)/;
    my $numa = $1;
    $b =~ /(\d+)/;
    my $numb = $1;

    return $numa <=> $numb;
}

sub parseInput
{
    foreach my $argNum ( 0 .. $#ARGV )
    {
        my $argStr = $ARGV[$argNum];

    }
}

sub displayWrapperBeginning
{

    my $includes = "\#include <iostream>\n";
    $includes .= "\#include <parseAttrOvd.H>\n";
    $includes .= "\#include <SpyInterface.H>\n";
    $includes .= "\#include <fapi2.H>\n";

    # include the correct ring id file base on chip name
    $includes .= "\#include <$RING_ID_FILE{$chip}>\n";

    $includes .= "\#include <p9_ec_func_cmsk_" . $chipId . "_" . $ec . ".H>\n";

    print OUTFILE $includes;

    print OUTFILE "\n";
    print OUTFILE "namespace fapi2 {\n    ReturnCode current_err\;\n}\n";

    print OUTFILE "\n\nint main (void)\n";
    print OUTFILE "{\n\n";
    print OUTFILE "fapi2::ReturnCode l_rc = fapi2::FAPI2_RC_SUCCESS\;\n";

    print OUTFILE "uint64_t l_tgtType_proc = (uint64_t)(fapi2::TARGET_TYPE_PROC_CHIP) << 32;\n";
    print OUTFILE
        "const fapi2::Target<fapi2::TARGET_TYPE_PROC_CHIP> l_tgt_proc(l_tgtType_proc | 0x0000000000ffffff);\n";

    displayFapiAttrSet( "ATTR_NAME", "l_tgt_proc", $CHIP_TYPE{$chipId} );
    displayFapiAttrSet( "ATTR_EC",   "l_tgt_proc", "0x$ec" );

    print OUTFILE "\nENGD::ChipEngData& l_engData = ENGD::ChipEngData::getInstance( $CHIP_TYPE{$chipId}, 0x$ec );\n";
    print OUTFILE "\nl_engData.init();\n";

}

sub displayWrapperBody
{
    my ( $chipId, $ec, $procedureName ) = @_;
    print OUTFILE "SPY::RingFactory::instance().clear()\;\n";

    #call FAPI HWP
    my $fapiExecHWP .= "FAPI_EXEC_HWP(l_rc," . $procedureName . ", l_tgt_proc);\n";

    $fapiExecHWP .= "if (l_rc)\n";
    $fapiExecHWP .= "{\n";
    $fapiExecHWP .= "FAPI_ERR(\"Error executing $procedureName\")\;\n";
    $fapiExecHWP .= "exit(l_rc)\;\n";
    $fapiExecHWP .= "}\n";
    print OUTFILE $fapiExecHWP;

    print OUTFILE
        "std::string fileName = \"../../output/gen/rings/hw/$chipId/$ec/runtime_base/ec_func_cmsk_ring.bin.srd\";\n";

    print OUTFILE "SPY::RingFactory::instance().dumpCmskRing(fileName,\n";
    print OUTFILE "l_ATTR_NAME, l_ATTR_EC);\n\n";

}

sub displayFapiAttrSet
{
    my ( $attribute, $target, $value ) = @_;
    my $attr_type = "fapi2::" . $attribute . "_Type";
    print OUTFILE "$attr_type l_$attribute = $value;\n";
    print OUTFILE "l_rc = ";
    print OUTFILE "fapi2::setAttrOvdValue(fapi2::$attribute, $target,";
    print OUTFILE "&l_$attribute, sizeof($attr_type));\n";
    print OUTFILE "if (l_rc)\n";
    print OUTFILE "{\n";
    print OUTFILE "    std::cerr << \"ERROR setting $attribute to $value\"";
    print OUTFILE " << std::endl;\n";
    print OUTFILE "    exit(-1);\n";
    print OUTFILE "}\n\n";
}

sub writeMkFile
{
    my ( $chipId, $ec, $outFile ) = @_;

    my $string .= "EXE=$outFile\n";
    $string    .= "\$(EXE)_TARGET=HOST\n";
    $string    .= "OBJS+=$outFile" . ".o\n";
    $string    .= "\$(EXE)_COMMONFLAGS+=-DFAPI_SUPPORT_SPY_AS_STRING\n";
    $string    .= "\$(EXE)_COMMONFLAGS+=-DPLAT_NO_THREAD_LOCAL_STORAGE\n";
    $string    .= "\$(EXE)_LIBPATH+=\$(LIBPATH)/ifCompiler\n";
    $string    .= "\$(EXE)_EXTRALIBS+=\$(ECMD_REQUIRED_LIBS)\n";
    $string    .= "\$(EXE)_DEPLIBS+=engd_parser parseAttrOvd fapi2_ifcompiler\n";

    $string .= "\$(EXE)_DEPLIBS+=p9_ec_func_cmsk_" . $chipId . "_" . $ec . "_ifCompiler\n";

    $string .= "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(GENPATH)/initfiles/wrapper/)\n";
    $string .= "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(GENPATH)/initfiles/$chipId/$ec/)\n";
    $string .= "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler)\n";
    $string .= "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(GENPATH)/fapi2_ifCompiler)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(GENPATH)/initfiles)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler/plat)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/utils/imageProcs)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/common/utils/imageProcs)\n";

    # Note: This ffdc is common for FAPI2, even though it's placed under chips/p9/... directory.
    # Leave it outside of the above block
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/procedures/hwp/ffdc)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler/engd_parser/include)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/hwpf/fapi2/include)\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ECMD_PLAT_INCLUDE))\n";
    $string .= "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/common/include/)\n";
    $string .= "\$(call BUILD_EXE,PRE_IMAGE)\n";
    print OUT_MK_FILE $string;
    close(OUT_MK_FILE);
}
