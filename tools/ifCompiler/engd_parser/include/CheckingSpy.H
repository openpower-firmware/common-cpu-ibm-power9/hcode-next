/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/engd_parser/include/CheckingSpy.H $          */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015,2016                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/**
 *  @file CheckingSpy.H
 *
 *  @brief This is the class that encapsulate a Checking spy, which
 *  is a special type of spy that requires ecc/parity checking
 *  when accessing.
 */

/**
 *  @page ChangeLogs Change Logs
 *  @section CHECKING_SPY_H CheckingSpy.H
 *  @verbatim
 */

#ifndef CHECKING_SPY_H
#define CHECKING_SPY_H

/*---------------------------------------------------------------------------*/
/*Includes                                                                   */
/*---------------------------------------------------------------------------*/
#include <variable_buffer.H>
#include <Spy.H>
#include <SpyCheckingGroup.H>

namespace SPY
{

/**
 * CheckingSpy is a special spy which defines ecc or parity checking
 *
 * Any checking spy consists of two sets of latches.
 * The first set are the input latches, which will be checked.
 * The second set are the output latches. They contain the result of the
 * different checks.
 * There are two standard ecc checking functions: Odd and Even parity checking.
 * And there are multiple specific ECC functions defined by the hardware team.
 * A checkingSpy is either an odd or even parity function, or its supports a specific
 * ecc function.
 * An ecc function is defined by a ecc table.
 * For each output bit for the ecc there is one row in the ecc table.
 * Each row consists of a flag (odd or even)and a bit mask.
 * To check and ecc, we have to walk through each row in the ecc table,
 * mask the input bits with the bit mask of the table, apply the odd or even
 * parity on the masked input bits and write the result into the output bits
 * (row n result = output bit n).
 *
 */
class CheckingSpy: public Spy
{

    public:

        /**
         * @brief Destructor for Checking Spy object.
         *
         * @par Description:
         * The default destructor of the CheckingSpy class.
         * This destructor will delete all buffers this object created.
         *
         * @return None.
         */
        virtual ~CheckingSpy();

        /**
         * @brief Default constructor for CheckingSpy object.
         *
         * @par Description
         * This is the default constructor for the CheckingSpy class
         *
         * @return None.
         */
        CheckingSpy(ruleSpyDataHandle*);

        /**
         * @brief Returns a spy's Checking Group Node
         *
         * @par Description:
         * This utility routine outputs the pointer to the Checking Group Node
         * of this Checking spy.
         *
         * @param[in]      i_nodeIndex          Node index
         * @param[out]     o_checkingGroupNode  Checking group node
         *
         * @return int
         */
        int getCheckingGroupNode(const uint32_t i_nodeIndex,
                                 SpyCheckingGroup*& o_checkingGroupNode) const;

        /**
          * @brief Initialize a CheckingSpy object.
          *
          * @par Description:
          * This routine initializes the CheckingSpy object according to
          * the specified input parameters.
          *
          * @param[in] i_spyOffset      This spy offset
          * @param[in] i_chipId         Chip hardware ID (extracted from IDEC)
          * @param[in] i_chipEcLevel    Chip EC level (extracted from IDEC)
          *
          * @return int
          */
        virtual int setData(const spyId_t i_spyOffset,
                            const uint8_t i_chipId,
                            const uint32_t i_chipEcLevel);

    private:
        SpyCheckingGroup* ivInOutGroupNodeArray;

};

} // End namespace SPY


#endif

