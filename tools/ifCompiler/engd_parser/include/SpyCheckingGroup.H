/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/engd_parser/include/SpyCheckingGroup.H $     */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */

/**
 *  @file SpyCheckingGroup.H
 *
 *  @brief This is the class that encapsulate the Checking groups
 *  within a Spy object.
 */

/**
 *  @page ChangeLogs Change Logs
 *  @section CHECKING_GROUP_H SpyCheckingGroup.H
 *  @verbatim
 */

#ifndef SPY_CHECKING_GROUP_H
#define SPY_CHECKING_GROUP_H

/*---------------------------------------------------------------------------*/
/*Includes                                                                   */
/*---------------------------------------------------------------------------*/
#include <SpyGroup.H>
#include <ruleSpyDataHandle.H>

namespace SPY
{

class SpyCheckingGroup: public SpyGroup
{

    public:
        /**
         * @brief Destructor for SpyCheckingGroup object.
         *
         * @par Description:
         * The default destructor of the SpyCheckingGroup class.
         * This destructor will delete all buffers this object created.
         *
         * @return None.
         */
        virtual ~SpyCheckingGroup();

        /**
         * @brief Default constructor for SpyCheckingGroup object.
         *
         * @par Description
         * This is the default constructor for the SpyCheckingGroup class
         *
         * @return None.
         */
        SpyCheckingGroup();

        /**
         * @brief Initialize a SpyCheckingGroup object.
         *
         * @par Description:
         * This function initializes the data for this SpyCheckingGroup
         * object based on information read from HWIL.
         *
         * @param[in] i_spyHandle   Pointer to instance spy data.
         * @param[in] i_spyOffset   The Spy offset
         * @param[in] i_groupIndex  The index of this group in the CheckingSpy
         *                          object
         * @param[in] i_numOfBits   Bit Length of this Checking group (In/Out bits)
         *
         * @return int
         *
         */
        int setData(ruleSpyDataHandle* i_spyHandle,
                    const spyId_t i_spyOffset,
                    const uint32_t i_groupIndex,
                    const uint16_t i_numOfBits);

    private:

        // The getRingMaps routine of RULE uses input parameter i_spyId
        // as either Spy ID or Spy offset.
        // If the spy is of normal type (Spy object), it expects the user
        // to pass in the value of the Spy's id.
        // If the spy is of Checking type (CheckingSpy), it expects the user
        // to pass in the value of the Spy offset.
        //
        // SpyGroup object, which is instantiated by non-checking type spies,
        // contains ivSpy value that is to be used for getRingMaps call.
        //
        // This SpyCheckingGroup object, which is instantiated by Checking type
        // spies (CheckingSpy), holds the offset value to be used for
        // getRingMaps call.
        spyId_t ivSpyOffset;

};

} // End namespace SPY


#endif

