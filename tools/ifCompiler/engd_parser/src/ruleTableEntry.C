/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/engd_parser/src/ruleTableEntry.C $           */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2001,2015                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#define ruleTableEntry_C

/**
 * This is an abstract class.  It should never actually be instantiated.
 *
 * The table entry classes for all the various table types are derived
 * from this class.  This class contains basic methods and attributes
 * common to all table entries, such as a file pointer and the table
 * entry search function.
 */


//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------
#include <ruleTableEntry.H>
#include <rule_common.H>
#include <assert.h>
#include <istream>
#include <iostream>
#include <string.h>
#include <arpa/inet.h>

//----------------------------------------------------------------------
// Global Variables
//----------------------------------------------------------------------

static const char this_class[] = "ruleTableEntry";
static const uint32_t this_file = 46;

//----------------------------------------------------------------------
// >>> Function <<<
//----------------------------------------------------------------------

// initialize static variables
std::list<ruleTableEntry::ruleFileHandlerStruct>
ruleTableEntry::cvListOfDescriptors;

char* ruleTableEntry::cvRulePath = NULL;


/**
 * Constructor.
 */
ruleTableEntry::ruleTableEntry():
    ivFilename("empty"), ivShortFilename("empty"), ivFileDescriptor(NULL),
    ivStartingSeekPos(0), ivCurrentSeekPos(0), ivNumRows(0), ivAllowNoFile(false),
    ivFileOpen(false)
{

    // initialize the static list of open files with a dummy descriptor
    if (cvListOfDescriptors.size() == 0)
    {
        ruleFileHandlerStruct newStruct;
        newStruct.fileDesc = new ruleFile;
        cvListOfDescriptors.push_back(newStruct);
    }
}


/**
 * Destructor.
 */
ruleTableEntry::~ruleTableEntry()
{
    // close the table file
    closeFile();
}

/**
 * Opens the specified file and sets the file pointer for this
 * class.  Returns a status code.
 *
 * @param string i_filename          Name of the table file to open
 * @param uint32_t i_supportedChecks Indicates what features this file supports
 * @param bool i_useRegistry         Indicates if the registry path should be
 *                                   used
 *
 * @return int Error handle
 */
int ruleTableEntry::openFile(std::string i_filename)
{
    openFileDesc( i_filename );
    return 0;
}

/**
 * Opens the specified file and/or increment the number of users for it.
 * set the instance variable to keep the fileDescriptor, so we don't have to
 * search for it again
 * Returns a status code.
 *
 * @param string i_filename          Name of the table file to open
 *
 * @return ruleFile File descriptor for the opened file
 */
ruleFile* ruleTableEntry::openFileDesc(std::string& i_filename)
{
    ivFileDescriptor = new ruleFile;
    ivFileDescriptor->open(i_filename.c_str());
    ivFileOpen = true;

    return ivFileDescriptor;
}


/**
 * Closes the file that this class currently has open.
 *
 * @return int Error handle
 */
int ruleTableEntry::closeFile()
{
    closeFileDesc();
    ivFilename = "empty";
    return 0;
}

/**
 * Decrement the number of users for this file, and close it if no more users
 * remain.
 *
 * @return uint32_t Return code
 */
uint32_t ruleTableEntry::closeFileDesc(void)
{

    std::list<ruleFileHandlerStruct>::iterator iter;

    //remove filedesc lookup and just close it
    if(ivFileDescriptor)
    {
        ivFileDescriptor->close();
        delete ivFileDescriptor;
        ivFileDescriptor = NULL;
    }

    ivFileOpen = false;

    return RULE_SUCCESS;
}



/**
 * Returns the filename
 *
 * @return string Filename
 */
std::string ruleTableEntry::getFile()
{
    return ivFilename;
}


/**
 * Returns the file descriptor
 *
 * @return ruleFile File access descriptor
 */
ruleFile* ruleTableEntry::getFileDesc(std::string& i_filename)
{

    bool found = false;

    // see if the provided filename is already open
    if( ivFileDescriptor != NULL)
    {
        found = true;
        ivFileDescriptor->clear();
        return ivFileDescriptor;
    }
    else
    {
        printf(
            "no file opened yet! <>%s.getFileDesc(file=%s,found=%d)", this_class,
            i_filename.c_str(), found);
        return (cvListOfDescriptors.back()).fileDesc;
    }
}


/**
 * Returns the number of rows in a table.
 *
 * @param uint32_t& o_numRows Number of rows in the table
 *
 * @return int Error handle
 */
int ruleTableEntry::getNumRows(uint32_t& o_numRows)
{
    int rc_out = -1;

    if (ivFilename.empty())
    {
        rc_out = RULE_FILESYSTEM_ERROR;

    }
    else
    {
        o_numRows = ivNumRows;
    }

    return rc_out;
}

/**
 * The function will take the given key and see if it matches the
 * one currently stored in the table entry.  If it matches, the
 * function returns.  If not, it will use the TableSearch function
 * to retrieve the requested data.
 *
 * This function returns the same status code regardless of whether
 * or not the data was retrieved from the table or already loaded.
 * If a file access error occurs, the return code reflects an
 * error.
 *
 * @param uint32_t i_keyval     Search key
 * @param uint32_t i_currentKey Currently loaded key
 * @param homUnitId i_tableType ID for the type of table being read
 *
 * @return int Error handle
 */
int ruleTableEntry::loadEntry(uint32_t i_keyval, uint32_t i_currentKey,
                              homUnitId i_tableType)
{
    printf("ruleTableEntry::loadEntry> Should execute the specific entry"
           "key:%d currentKey:%d tableType:%d\n",
           i_keyval, i_currentKey, i_tableType);
    assert(0);
    return -1;
}



/**
 * The function will take the given key and see if it matches the
 * one currently stored in the table entry.  If it matches, the
 * function returns.  If not, it will use the TableSearch function
 * to retrieve the requested data.
 *
 * This function returns the same status code regardless of whether
 * or not the data was retrieved from the table or already loaded.
 * If a file access error occurs, the return code reflects an
 * error.
 *
 * @param uint32_t i_keyval     Search key
 * @param uint32_t i_currentKey Currently loaded key
 * @param homUnitId i_tableType ID for the type of table being read
 *
 * @return int Error handle
 */
int ruleTableEntry::loadEntry(uint32_t i_keyval, uint32_t i_currentKey,
                              homUnitId i_tableType, uint32_t i_offset)
{
    printf("ruleTableEntry::loadEntry> Should execute the specific entry"
           "key:%d currentKey:%d tableType:%d offset:%d\n",
           i_keyval, i_currentKey, i_tableType, i_offset);
    assert(0);
    return -1;
}




/**
 * Returns the seek position of the start of the next table entry
 * after the one provided.  Assumes that the provided seek position
 * points to the START of a valid table entry.
 *
 * @param uint32_t i_currPos Current seek position in the file
 * @param uint32_t i_fixedSize Size in bytes of the fixed fields in the entry
 * @param uint32_t i_numVariable Number of variable length fields
 *
 * @return uint32_t Starting seek position of next entry
 */
uint32_t ruleTableEntry::advanceToNextEntry(uint32_t i_currPos,
        uint32_t i_fixedSize,
        uint32_t i_numVariable)
{
    uint32_t tempsize = 0, i = 0, newseek = 0;
    ruleFile* fileHandle = getFileDesc(ivFilename);

    // seek past the fixed part of the entry
    newseek = fileHandle->seekg(i_currPos + i_fixedSize);

    // iterate through the current table entry's remaining variable fields
    for(i = 0; i < i_numVariable; i++)
    {
        fileHandle->read(reinterpret_cast<char*>(&tempsize), sizeof(uint32_t));
        newseek = fileHandle->seekg(tempsize, std::ios::cur);
    }

    if (fileHandle->fail() || fileHandle->eof())
    {
        newseek = 0xffffffff;
    }

    return newseek;
}


/**
 * This is the standard table search function.  This function will
 * take as parameters a 32-bit key, the offset in the table entry of
 * the field to be searched, the size of a table entry, and an
 * initial seek position.  It will return the seek position in the
 * table where the first incidence (after the given seek position)
 * of the given key was found.  If the key was not found, the
 * function returns -1.
 *
 * If the offset in the table entry of the field to be searched is
 * not zero, this function will halt a search if the first part of
 * the table entries searched changes.  For example, if we've
 * already searched for the start location of entries with a
 * particular Key1, and we start looking for a particular Key2, we
 * do this check to make sure that we don't accidentally walk to a
 * part of the table where Key1 is no longer valid.
 *
 * @param uint32_t i_key       Key value to search for
 * @param uint32_t i_offset    Offset from the start of the entry to look
 * @param uint32_t i_startSeek Starting seek position in the file
 * @param bool     i_skipCheck TRUE=skip check of verified keys
 *
 * @return int Error handle
 */
uint32_t ruleTableEntry::searchTable(uint32_t i_key,
                                     uint32_t i_offset,
                                     uint32_t i_startSeek,
                                     bool i_skipCheck)
{
    ruleFile* fileHandle = getFileDesc(ivFilename);
    fileHandle->seekg(i_startSeek);
    char* verifiedKeys = NULL;
    char* precedingKeys = NULL;
    int flag = 0; //verified keys don't exist flag

    // Read in keys verified so far, if any
    if (i_offset != 0)
    {
        verifiedKeys =  new char[i_offset];
        precedingKeys = new char[i_offset];
        fileHandle->read(verifiedKeys, i_offset);
    }

    // Initialize currentKey to something other than the key being searched
    uint32_t currentKey = i_key + 1;

    fileHandle->seekg(i_startSeek);
    uint32_t currentSeek = i_startSeek;

    while (!fileHandle->eof() && !fileHandle->fail())
    {
        flag = 0; //reinitialize flag

        // Read the preceding keys and check if they match, if needed
        if (i_offset != 0)
        {
            fileHandle->read(precedingKeys, i_offset);
        }

        // Check if this entry has the right key
        fileHandle->read(reinterpret_cast<char*>(&currentKey),
                         sizeof(uint32_t));

        if (currentKey == i_key)
        {
            if (verifiedKeys != NULL && !i_skipCheck)
            {
                if (memcmp(verifiedKeys, precedingKeys, i_offset))
                {
                    flag = 1;
                }
            }

            if (!flag)
            {
                // Found!
                break;
            }
        }

        // Advance the seek position to the start of the next entry
        currentSeek = advanceToNextEntry(currentSeek);

        if (currentSeek == 0xffffffff)
        {
            break;
        }
    }

    // Check if the key was never found
    if (fileHandle->eof() || fileHandle->fail())
    {
        fileHandle->clear();
        currentSeek = 0xffffffff;
    }

    // Cleanup
    if (verifiedKeys != NULL)
    {
        delete[] verifiedKeys;
    }

    if (precedingKeys != NULL)
    {
        delete[] precedingKeys;
    }

    return currentSeek;
}


/**
 * This is the standard table search function.  This function will
 * take as parameters a 64-bit key, the offset in the table entry of
 * the field to be searched, the size of a table entry, and an
 * initial seek position.  It will return the seek position in the
 * table where the first incidence (after the given seek position)
 * of the given key was found.  If the key was not found, the
 * function returns -1.
 *
 * If the offset in the table entry of the field to be searched is
 * not zero, this function will halt a search if the first part of
 * the table entries searched changes.  For example, if we've
 * already searched for the start location of entries with a
 * particular Key1, and we start looking for a particular Key2, we
 * do this check to make sure that we don't accidentally walk to a
 * part of the table where Key1 is no longer valid.
 *
 * @param uint64_t i_key       Key value to search for
 * @param uint32_t i_offset    Offset from the start of the entry to look
 * @param uint32_t i_startSeek Starting seek position in the file
 * @param bool     i_skipCheck TRUE=skip check of verified keys
 *
 * @return int Error handle
 */
uint32_t ruleTableEntry::searchTable(uint64_t i_key,
                                     uint32_t i_offset,
                                     uint32_t i_startSeek,
                                     bool i_skipCheck)
{
    ruleFile* fileHandle = getFileDesc(ivFilename);
    fileHandle->seekg(i_startSeek);
    char* verifiedKeys = NULL;
    char* precedingKeys = NULL;
    int flag = 0; //verified keys don't exist flag

    // Read in keys verified so far, if any
    if (i_offset != 0)
    {
        verifiedKeys =  new char[i_offset];
        precedingKeys = new char[i_offset];
        fileHandle->read(verifiedKeys, i_offset);
    }

    // Initialize currentKey to something other than the key being searched
    uint64_t currentKey = i_key + 1;

    fileHandle->seekg(i_startSeek);
    uint32_t currentSeek = i_startSeek;

    while (!fileHandle->eof() && !fileHandle->fail())
    {
        flag = 0;

        // Read the preceding keys and check if they match, if needed
        if (i_offset != 0)
        {
            fileHandle->read(precedingKeys, i_offset);
        }

        // Check if this entry has the right key
        fileHandle->read(reinterpret_cast<char*>(&currentKey),
                         sizeof(uint64_t));

        if (currentKey == i_key)
        {

            if (verifiedKeys != NULL && !i_skipCheck)
            {
                if (memcmp(verifiedKeys, precedingKeys, i_offset))
                {
                    flag = 1;
                }
            }

            if (!flag)
            {
                // Found!
                break;
            }
        }

        // Advance the seek position to the start of the next entry
        currentSeek = advanceToNextEntry(currentSeek);

        if (currentSeek == 0xffffffff)
        {
            break;
        }

    }

    // Check if the key was never found
    if (fileHandle->eof() || fileHandle->fail())
    {
        fileHandle->clear();
        currentSeek = 0xffffffff;
    }

    // Cleanup
    if (verifiedKeys != NULL)
    {
        delete[] verifiedKeys;
    }

    if (precedingKeys != NULL)
    {
        delete[] precedingKeys;
    }

    return currentSeek;
}






/*
 * This routine searches the table from the input file position (i.e. table
 * entry's row) to see if the values at the specified input positions match
 * to the input values.
 *
 * NOTE: This routine assumes each entry is an uint32_t data type.
 *
 * If there's a match, this routine will return the seek position in
 * the table where the first incidence (after the given seek position)
 * of the given key was found.  Otherwise, the function returns -1.
 *
 * @param uint32_t  i_valToMatch      Array of values to be matched to
 * @param uint32_t  i_keyPos          Array of positions of keys
 * @param uint32_t  i_numberOfKeys    Number of keys to be matched
 * @param uint32_t  i_startSeek       Starting seek position in the file
 *
 * @return int Error handle
 */
uint32_t ruleTableEntry::searchTableEntries(uint32_t  i_valToMatch[],
        uint32_t  i_keyPos[],
        uint32_t  i_numOfKeys,
        uint32_t  i_startSeek)
{
    uint32_t index = 0;
    uint32_t fileKeyVal = 0xFFFFFFFF;
    ruleFile* fileHandle = getFileDesc(ivFilename);
    uint32_t currentSeek = i_startSeek; // Init search table row file position

    // Point to the first key of search row  //@11
    fileHandle->seekg(currentSeek);

    // Loop thru each table entry
    while ( !fileHandle->eof() && !fileHandle->fail() )
    {

        // Point to the first key of search row
        //fileHandle->seekg(currentSeek); //@11

        // Set keySeek to first key of this row
        uint32_t keySeek = currentSeek;

        // Read in the specified values from this table's row
        for (index = 0; index < i_numOfKeys; index++)
        {

            // Advance file pointer to the correct key position
            keySeek += ( (i_keyPos[index]) * sizeof(uint32_t) );
            fileHandle->seekg(keySeek);

            // ..and read in the value
            fileHandle->read(reinterpret_cast<char*>(&fileKeyVal),
                             sizeof(uint32_t));

            // If value matched, continue loop for next key comparision
            if ( fileKeyVal == i_valToMatch[index] )
            {
                keySeek = currentSeek; // Init back to begin of row
            }
            else   // No match, advance to next row
            {
                break;
            }
        }

        // If all values are checked, then we have a match
        if (index == i_numOfKeys)
        {
            break;
        }
        // This table row isn't it, advance to the next table row
        else
        {
            currentSeek = advanceToNextEntry(currentSeek);

            if (currentSeek == 0xffffffff)     // We are at the end
            {
                break; // Get out
            }
        }
    }

    if ( fileHandle->eof() || fileHandle->fail() )
    {
        fileHandle->clear();
        currentSeek = 0xffffffff;
    }

    return currentSeek;

}


uint64_t ruleTableEntry::rule_ntohll( uint64_t i_val )
{
    uint32_t* first = (uint32_t*)&i_val;

    return ((static_cast<uint64_t>( ntohl(*first)) << 32 )
            + (ntohl(static_cast<uint32_t>(i_val >> 32))));

}
