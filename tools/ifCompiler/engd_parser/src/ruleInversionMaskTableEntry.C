/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/engd_parser/src/ruleInversionMaskTableEntry.C $ */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#define ruleInversionMaskTableEntry_C

/**
 * Each scanring has an inversion mask which needs to be applied for
 * every access. The inversion mask consists of an id, a length and the
 * actual bit string.
 * These bit strings of the masks differ in length. The table entries of
 * this table will be NOT fixed, but flexible.
 *
 * There will be one inversion mask table for every chipid/ec.
 *
 * If the size of this table becomes an issue, we have the option to
 * store it in a compressed form. In this case we would store only the
 * places where a 1 should be set.
 */


//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------

#include <ruleInversionMaskTableEntry.H>
#include <stdio.h>
#include <netinet/in.h>
#include <assert.h>

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------


//----------------------------------------------------------------------
// >>> Function <<<
//----------------------------------------------------------------------

/**
 * Default constructor.
 */
ruleInversionMaskTableEntry::ruleInversionMaskTableEntry()
{

}


/**
 * Destructor.
 */
ruleInversionMaskTableEntry::~ruleInversionMaskTableEntry()
{

}


/**
 * Opens the specified file and sets the file pointer for this
 * class.  Returns a status code.
 *
 * This is basically identical to the version implemented in ruleTableEntry,
 * except this version does not do the HOM_LAST_UNITID check that the other
 * one does, nor does it count rows since there aren't really any.
 *
 * @param string i_filename  Name of the table file to open
 *
 * @return int Error handle
 */
int ruleInversionMaskTableEntry::openFile(std::string i_filename)
{
    return ruleTableEntry::openFile(i_filename);
}


/**
 * This function is NOT IMPLEMENTED.  There is no nead to copy table entry
 * data for inversion masks.
 *
 * @param uint32_t i_filePos Position in the file to load from
 *
 * @return hutlErrHandl_t Error handle
 */
int ruleInversionMaskTableEntry::copyData(uint32_t i_filePos)
{
    printf("There is no need to copy data from %d for inversion masks\n",
           i_filePos);
    assert(0);

    return 0;
}


/**
 * This is a virtual function.  This function is NOT IMPLEMENTED here.  There
 * is no need to advance table entries for inversion masks.
 *
 * @param uint32_t i_currPos Seek position of the current entry
 *
 * @return uint32_t Seek position of the next entry
 */
uint32_t ruleInversionMaskTableEntry::advanceToNextEntry(uint32_t i_currPos)
{
    printf("There is no need to advance enteries for inversion mask."
           " curPos:%d\n", i_currPos);
    assert(0);

    return 0xffffffff;
}


/**
 * Returns the inversion mask in a bit buffer.
 *
 * @param uint32_t i_offset       Offset to the inv mask being requested
 * @param uint23_t i_length       Length of the provided buffer
 * @param ecmdDataBuffer* o_buffer Buffer to write the inversion mask to
 *
 * @return int Error handle
 */
int ruleInversionMaskTableEntry::getInvMask(uint32_t i_offset,
        uint32_t i_length,
        uint8_t i_optimized,
        ecmdDataBuffer* o_buffer)
{
    uint32_t rc = 0;
    uint32_t i = 0;

    if( i_length == 0 )
    {
        printf("no inversion mask data just return\n");
        return 0;
    }

    ruleFile* fileHandle = getFileDesc(ivFilename);

    fileHandle->seekg(i_offset);

    // Allocate the buffer space and read in the mask data
    uint32_t* bufptr = new uint32_t[i_length];
    fileHandle->read(reinterpret_cast<char*>(bufptr), i_length * sizeof(uint32_t));

    if(i_optimized)
    {
        //ak2
        uint32_t wordOffset;
        uint32_t invmaskValue;

        if(i_length % 2 == 0)
        {
            for (i = 0; i < i_length; i += 2)
            {
                wordOffset = ntohl(bufptr[i]);
                invmaskValue = ntohl(bufptr[i + 1]); /*accessing beyond memory*/
                rc |= o_buffer->setWord(wordOffset, invmaskValue);
            }
        }
        else
        {

            printf("Word offset and invermask pair not found\n");

            rc = -1;

        }
    }
    else
    {
        //need to byte swap this data when reading in
        for(i = 0; i < i_length; i++)
        {
            bufptr[i] = ntohl(bufptr[i]);
        }

        rc = o_buffer->memCopyIn(bufptr, sizeof(uint32_t) * i_length);

    }

    delete [] bufptr;

    if (!rc && (fileHandle->eof() || fileHandle->fail()))
    {
        fileHandle->clear();

        printf(
            "E-ruleInversionMaskTableEntry.getInvMask:eof or file "
            "read failure for %s",
            ivFilename.c_str());
        rc = RULE_FILESYSTEM_ERROR;

    }

    return rc;
}

