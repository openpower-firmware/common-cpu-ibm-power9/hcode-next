# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/imageProcs/ring_apply/p9_ring_apply.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

###############################################################################
#
# Pseudo code of this makefile
#
# foreach chip (p9, centaur, ocmb)  Note: centaur and ocmb are filtered out
#
#   p9
#
#     $(foreach id,$($(chip)_CHIPID) (p9n, p9c, p9a)
#
#       p9n
#
#         $(foreach mode,$(HW_IMAGE_VARIATIONS)  (hw, sim)
#
#           hw
#
#              if  $(chip):$($id) exists (p9:p9n, p9:p9c, centaur:cen) in CHIP_EC_PAIRS
#              (Only call RUN_RING_APPLY for chips/ids that have ENGD loaded)
#
#              call RUN_RING_APPLY $id, $mode (p9n, hw)
#
#                 GENERATED = run_ring_apply_p9n_hw
#
#                 $(foreach ec, $($(1)_EC)  (10, 20)
#
#                     ec = 10
#
#                         call ADD_RING_APPLY_SOURCES $id, $mode, $ec (p9n, hw, 10)
#                             SOURCES+=../../output/gen/.gen_ring_p9n_10_hw.built
#
#                     ec = 20
#
#                         call ADD_RING_APPLY_SOURCES $id, $mode, $ec (p9n, hw, 20)
#                             SOURCES+=../../output/gen/.gen_ring_p9n_10_hw.built  ../../output/gen/.gen_ring_p9n_20_hw.built
#                 End foreach ec loop
#
#                 DD_LEVELS = --dd 10 --dd 20
#                 TARGETS   = p9n.hw.rings.bin
#                 GENERATED = run_ring_apply_p9n_hw
#                 GENERATED_COMMAND_PATH = ../../output/bin/
#                 GENERATED_PATH: ../../output/gen/rings/hw
#
#                 call RUN_RING_APPLY_COMMAND  $id, $mode (p9n, hw)
#
#                     RUN_RING_APPLY_COMMAND: CHIP: p9n, DD_LEVELS: --dd 10 --dd 20
#                     RUN_RING_APPLY_COMMAND: GENERATED: run_ring_apply_p9n_hw, TARGETS: p9n.hw.rings.bin
#                     RUN_RING_APPLY_COMMAND: GENERATED_PATH: ../../output/gen/rings/hw
#                     RUN_RING_APPLY_COMMAND: GENPATH: ../../output/gen
#
#	       end if condition
#
#           sim
#
#                Same as above, with 'sim' instead of 'hw'
#
#       p9c
#
#                Same as above, with 'p9c instead of 'p9n'
#
#     End id loop
#
#   centaur (currently disabled)
#
#     $(foreach id,$($(chip)_CHIPID) (cen)
#
#       cen
#
#         $(foreach mode,$(HW_IMAGE_VARIATIONS)  (hw, sim)
#
#           hw
#               .....
#
#           sim
#
# End chip loop
#
###############################################################################

###############################################################################
# BUILD_RING_APPLY
###############################################################################
define BUILD_RING_APPLY
$(eval EXE = p9_ring_apply)
$(eval $(EXE)_EXTRALIBS+=$(ECMD_REQUIRED_LIBS))
$(eval $(EXE)_DEPLIBS += common_ringId)
$(eval $(EXE)_DEPLIBS += p9_ringId)
$(eval $(EXE)_DEPLIBS += cen_ringId)
$(eval $(EXE)_DEPLIBS += p9_ring_identification)
$(eval $(EXE)_DEPLIBS += p9_scan_compression)
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ECMD_PLAT_INCLUDE)))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/chips/common/utils/imageProcs))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/chips/p9/utils/imageProcs))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/chips/centaur/utils/imageProcs))
$(eval $(call ADD_EXE_INCDIR,$(EXE),$(ROOTPATH)/hwpf/fapi2/include))
$(eval OBJS=p9_ring_apply.o)
$(call BUILD_EXE)
endef

###############################################################################
# ADD_RING_APPLY_SOURCES
#
#     $1 = chip ID (p9n, p9c, cen)
#     $2 = mode (hw, sim)
#     $3 = ec
#
###############################################################################
define ADD_RING_APPLY_SOURCES
$(eval SOURCES+=$(GENPATH)/.gen_rings_$(1)_$(3)_$(2).built)
$(eval $(if $(findstring $1,cen),,SOURCES+=$(GENPATH)/.gen_cmsk_ring_$(1)_$(3).built))
endef


###############################################################################
#$1 == chip ID
#$2 == mode
#$3 == ringSection (.rings, .overrides, .overlays)
#$4 == outputfile name
define RUN_RING_APPLY_COMMAND
	$(C1) $$$$< \
		--ringSection .$3 --chip $(1) $(DD_LEVELS) \
		--outFile $$($(GENERATED)_PATH)/$(4) --inputFilePath $(GENPATH)/rings/$2/
endef

###############################################################################
# RUN_RING_APPLY
#
#
# This macro will build a .rings, .overrides or .overlays ring section based on the 
# ringSection type passed in:
#  if ringSection == .rings then a <chipId>.<mode>.rings.bin file will be created
#  if ringSection == .overrides then a <chipId>.<mode>.overrides.bin file will be created
#  if ringSection == .overlays a <chipId>.<mode>.overlays.ddXX will be created,
#  for each supported EC level where XX is the dd level
#  the overlays sections will then be stiched together by another rule to create a
#  <chipId>.<mode>.overlays.bin (.overlays section) to be apended to the hw_image
#
#     $1 == $chip ID (p9n, p9c, cen)
#     $2 == $mode (hw, sim)
#     $3 == $ringSection (.rings, .overrides, .overlays)
###############################################################################
define RUN_RING_APPLY
$(eval GENERATED=run_ring_apply_$1_$2_$3)
DD_LEVELS:=
SOURCES:=
$(foreach ec, $($(1)_EC),\
	$(eval DD_LEVELS+= --dd $(ec))\
	$(eval $(call ADD_RING_APPLY_SOURCES,$1,$2,$(ec),$3)))
$(eval TARGETS=$(1).$(2).$(3))
$(eval $(GENERATED)_COMMAND_PATH=$(EXEPATH)/)
$(eval COMMAND=p9_ring_apply.exe)
$(eval $(GENERATED)_PATH=$(GENPATH)/rings/$2)
$(eval $(GENERATED)_RUN=$(call RUN_RING_APPLY_COMMAND,$1,$2,$3,$(TARGETS)))
$(call BUILD_GENERATED,IMAGE)
endef

###########################################################################################
# Generate the rules to run the the generic dd level container tool to create the
# .overlays section
#
# $1 == $chipId (p9n,p9c,cen)
# $2 == $mode   (hw,sim)
# $3 == $ringSection (.rings, .overrides, .overlays)
#
# GENERATED_PATH=obj/gen/rings/$(2)
#
# Depends on:
# 	.run_ring_apply_$1_$2_$3.built (generated by RUN_RING_APPLY)
#
# Generates:
# 	.run_dd_container_tool_$1_$2_$3.built (.run_dd_container_tool_p9n_hw_overlays.built)
#
define GENERATE_RING_SECTION
DD_BLOCKS=
SOURCES=
$(eval  GENERATED=run_dd_container_tool_$1_$2_$3)
$(eval $(GENERATED)_PATH=$(GENPATH)/rings/$(2))
$(foreach ec, $($(1)_EC),\
	$(eval DD_BLOCKS+= --dd 0x$(ec) --block $($(GENERATED)_PATH)/$(1).$(2).$(3).dd$(ec).bin))
$(eval SOURCES+=$($(GENERATED)_PATH)/.run_ring_apply_$(1)_$(2)_$(3).built)
$(eval TARGETS=$(1).$(2).$(3).bin)
$(eval $(GENERATED)_COMMAND_PATH=$(EXEPATH)/)
$(eval COMMAND=p9_dd_container_tool.exe)
$(eval $(GENERATED)_RUN=$(call RUN_DD_CONTAINER_TOOL_COMMAND,$(DD_BLOCKS),$($(GENERATED)_PATH)/$(TARGETS)))
$(call BUILD_GENERATED,IMAGE)
endef

#################################################################################################
# Recipe to run the dd container creation tool
# $1 == dd levels and block info (eg. --dd 0x10 --block p9n.hw.overlays.dd10.bin )
# $2 == container to create (fully qualified filename)
define RUN_DD_CONTAINER_TOOL_COMMAND
	$(C1) $$$$< --command add --cont $(2) $(1)
endef

###############################################################################
define GENERATE_RINGS
$(eval $(call RUN_RING_APPLY,$(1),$(2),$(3)))
$(eval $(call GENERATE_RING_SECTION,$(1),$(2),$(3)))
endef

###############################################################################
$(eval $(call BUILD_RING_APPLY))

# For HW image .rings/.overlays for p9n/c/a
RING_SECTIONS=rings overlays
$(eval CHIP_TYPE=$(filter p9,$(CHIPS)))
$(foreach ringsection,$(RING_SECTIONS),\
	$(foreach mode,$(HW_IMAGE_VARIATIONS),\
	$(foreach chip,$(CHIP_TYPE),\
	    $(foreach id,$($(chip)_CHIPID),\
			    $(if $(findstring $(chip):$($id), $(CHIP_EC_PAIRS)),\
			    $(eval $(call GENERATE_RINGS,$(id),$(mode),$(ringsection))))))))

# For Centaur image .rings
RING_SECTIONS=rings
$(eval CHIP_TYPE=$(filter centaur,$(CHIPS)))
$(foreach ringsection,$(RING_SECTIONS),\
	$(foreach mode,$(HW_IMAGE_VARIATIONS),\
	$(foreach chip,$(CHIP_TYPE),\
	    $(foreach id,$($(chip)_CHIPID),\
			$(if $(findstring $(chip):$($id), $(CHIP_EC_PAIRS)),\
			$(eval $(call GENERATE_RINGS,$(id),$(mode),$(ringsection))))))))

