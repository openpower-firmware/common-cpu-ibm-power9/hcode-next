# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/fapi.dir/wrapper.rules.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Makefile that defines how we build 'wrappers', which are a special kind of
# executable recognized by Cronus.

# BUILD_WRAPPER
#    This macro will automatically generate all the recipes for building a
#    wrapper.  Requires that the following variables are defined prior to
#    calling this macro:
#        * WRAPPER=name - Name of the wrapper.  name.o is an implied
#                           required object.
#        * FAPI=version - Optional method to specify the FAPI version.
define BUILD_WRAPPER
$(eval $(call __BUILD_WRAPPER)) \
$(call BUILD_EXE) \
$(eval WRAPPER:=) \
$(eval FAPI:=)
endef

# Order of operations:
#    * Assign EXE=$(WRAPPER).
#    * Add $(WRAPPER).o to the required objects.
#    * Add the $(GENPATH) to the source directory.
#    * Call the macro for the FAPI1 or FAPI2 specific flags.
#    * Call the macro for the PRCD specific flags.
#    * Add each library's INCDIRS from $(WRAPPER)_USELIBS to the
#         wrapper's INCDIR.
define __BUILD_WRAPPER
EXE = $(WRAPPER)
OBJS += $(WRAPPER).o
$(call ADD_EXE_SRCDIR,$(WRAPPER),$(GENPATH))
$(if $(filter $(FAPI),1),$(call FAPI1_WRAPPER),$(call FAPI2_WRAPPER))
$(call PRCD_WRAPPER)
$(foreach lib,$($(WRAPPER)_USELIBS),\
	$(call ADD_EXE_INCDIR,$(WRAPPER),$(lib$(lib)_INCDIRS)))
$(WRAPPER)_DEPSORDER += $(foreach lib,$($(WRAPPER)_USELIBS),$(LIBPATH)/lib$(lib).a)
endef

# Settings needed for FAPI1 wrappers.
define FAPI1_WRAPPER
$(warning "WARNING: We don't know how to build FAPI1 yet for $(WRAPPER)")
endef

# Settings needed for FAPI2 wrappers.
#    * Add fapi2 framework include path.
#    * Add Cronus inclue paths.
#    * Add dependency for fapi2 module.
#    * Add dependency on Cronus libraries.
#    * Add dependency on libdl.so.
define FAPI2_WRAPPER
$(call __ADD_EXE_INCDIR,$(WRAPPER),$(FAPI2_PATH)/include)
$(call __ADD_EXE_INCDIR,$(WRAPPER),$(FAPI2_PLAT_INCLUDE))
$(WRAPPER)_EXTRALIBS += $(FAPI2_REQUIRED_LIBS)
$(WRAPPER)_DEPLIBS += fapi2_utils
$(WRAPPER)_LDFLAGS += -ldl
endef

# Settings needed for PRCD wrappers.
#    * Add prcd framework include path.
#    * Add Cronus include paths.
#    * Add depndency for the prcd module.
#    * Add dependency on Cronus libraries.
#    * Add dependency on libdl.so.
define PRCD_WRAPPER
$(call __ADD_EXE_INCDIR,$(WRAPPER),$(PRCD_PATH))
$(call __ADD_EXE_INCDIR,$(WRAPPER),$(PRCD_PLAT_INCLUDE))
$(WRAPPER)_DEPLIBS += prcd
$(WRAPPER)_EXTRALIBS += $(PRCD_REQUIRED_LIBS)
$(WRAPPER)_LDFLAGS += -ldl
endef
