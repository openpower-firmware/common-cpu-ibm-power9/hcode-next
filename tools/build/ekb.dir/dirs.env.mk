# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/dirs.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Makefile to define additional paths to find global makefiles.

# Pick up fapi2 makefiles.
ifeq ($(UNAME),Linux)
MAKEFILE_PATH += $(ROOTPATH)/hwpf/fapi2/src
MAKEFILE_PATH += $(ROOTPATH)/hwpf/fapi2/tools
MAKEFILE_PATH += $(ROOTPATH)/hwpf/fapi2/test
MAKEFILE_PATH += $(ROOTPATH)/hwpf/prcd
MAKEFILE_PATH += $(ROOTPATH)/tools/ifCompiler/initCompiler
MAKEFILE_PATH += $(ROOTPATH)/tools/ifCompiler/engd_parser
MAKEFILE_PATH += $(ROOTPATH)/tools/ifCompiler/plat
MAKEFILE_PATH += $(ROOTPATH)/tools/ifCompiler
MAKEFILE_PATH += $(GENPATH)/initfiles/wrapper
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/utils/fir_checking
MAKEFILE_PATH += $(ROOTPATH)/chips/centaur/procedures/utils/fir_checking
MAKEFILE_PATH += $(ROOTPATH)/tools/imageProcs/ring_apply
MAKEFILE_PATH += $(ROOTPATH)/tools/genOverrideImage/
MAKEFILE_PATH += $(ROOTPATH)/tools/genOverrideImage/test
MAKEFILE_PATH += $(ROOTPATH)/tools/genOverrideImage/test/wrapper
MAKEFILE_PATH += $(ROOTPATH)/generic/memory/lib

# pick up the cmsk procedures
MAKEFILE_PATH+= $(shell find $(ROOTPATH)/output -path *initfiles/*/* -type d)

# Pick up procedure, test, wrapper makefiles.
EKB_CHIP_UNITS = $(foreach chip,$(CHIPS),$(wildcard $(ROOTPATH)/chips/$(chip)/procedures/hwp/*))


MAKEFILE_PATH += $(EKB_CHIP_UNITS)
MAKEFILE_PATH += $(addsuffix /tests,$(EKB_CHIP_UNITS))
MAKEFILE_PATH += $(addsuffix /wrapper,$(EKB_CHIP_UNITS))
MAKEFILE_PATH += $(addsuffix /lib,$(EKB_CHIP_UNITS))

# Pick up vpd accessors after other centaur directories
MAKEFILE_PATH += $(ROOTPATH)/chips/centaur/procedures/vpd_accessors

MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(wildcard $(ROOTPATH)/chips/$(chip)/xip))

# Pick up utility, wrapper makefiles
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/utils)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/utils/sbeUtils)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/utils/wrapper)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/utils/FabricTraceFormatter)
# Pick up VBU specific procedure makefiles
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe/tools/ppetracepp
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/vbu/sim)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(wildcard $(ROOTPATH)/chips/$(chip)/procedures/vbu/sim/*))

# Pick up scominfo code
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/common/scominfo)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/common/scominfo/wrapper)

# Pick up image build makefiles
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/cme/stop_cme
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/cme
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/sgpe/stop_gpe
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/sgpe/boot
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/pgpe/pstate_gpe
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/pgpe/boot
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/ippe/ionv
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/ippe/iox
MAKEFILE_PATH += $(ROOTPATH)/chips/p9/procedures/ppe_closed/ippe/ioa
MAKEFILE_PATH += $(ROOTPATH)/tools/imageProcs
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(wildcard $(ROOTPATH)/chips/$(chip)/utils/imageProcs))
endif

#We intend to build only STOP API Tool on AIX. Code in any other path should not compile.
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/utils/stopreg)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/hwp/pm/tools/stopApi)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/hwp/pm/tools/stop_recovery/)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/hwp/pm/tools/extHom)
MAKEFILE_PATH += $(foreach chip,$(CHIPS),$(ROOTPATH)/chips/$(chip)/procedures/hwp/pm/tools/err_Injector)
