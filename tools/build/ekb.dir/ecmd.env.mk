# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/ecmd.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
#
# ECMD paths from Cronus.
ECMD_PLAT_INCLUDE += $(CTEPATH)/tools/ecmd/$(ECMD_RELEASE)/capi
ECMD_PLAT_LIB = $(CTEPATH)/tools/ecmd/$(ECMD_RELEASE)/x86_64/lib

# Extra libs to link against for Cronus
ECMD_REQUIRED_LIBS += $(ECMD_PLAT_LIB)/ecmdClientCapi.a
ECMD_REQUIRED_LIBS += $(ECMD_PLAT_LIB)/libecmd.so
ECMD_REQUIRED_LIBS += $(ECMD_PLAT_LIB)/croClientCapi.a
