# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/prcd.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Makefile to define all the variables and paths for building PRCD under
# Cronus

# Path to PRCD code.
PRCD_PATH = $(ROOTPATH)/hwpf/prcd/

# PRCD paths from Cronus.
PRCD_PLAT_INCLUDE += $(CTEPATH)/tools/ecmd/$(ECMD_RELEASE)/ext/cro/capi
PRCD_PLAT_INCLUDE += $(ECMD_PLAT_INCLUDE)

# Extra libraries to link against for Cronus.
PRCD_REQUIRED_LIBS += $(ECMD_REQUIRED_LIBS)
