This repository is for engineering data for Power Architecture chips.

This repository is currently in an evaluation phase. Its contents and structure could change suddenly and without notice.

Explanation of directories.
p9n - files here relate to Power9 Nimbus technology
p9n/21 - engineering data for EC 2.1
p9n/22 - enginerring data for EC 2.2
